package Cookie;

require Exporter;
use strict;
use warnings;
our @ISA = qw(Exporter);
use DBI;

our @EXPORT = qw();
our @EXPORT_OK = qw();


# Path, cookies are for:
my $cookie_path = "/";
# Expiration dates:
my $expiration_keep = "Thu, 01-Jan-2020 00:00:00 GMT";
my $expiration_dele = "Thu, 01-Jan-1970 00:00:00 GMT";





# Print a http-header line with a cookie:
sub set($$) {
	my($name, $value) = @_;
	print "Set-Cookie: ";
	print ($name, "=", $value, "; expires=", $expiration_keep, "; path=", $cookie_path, "\n");
}

# Print a http-header line to remove a cookie:
sub del($) {
	my($name) = @_;
	print "Set-Cookie: ";
	print ($name, "=; expires=", $expiration_dele, "; path=", $cookie_path, "\n");
}

# Get one cookie;
sub get($) {
	my($name) = @_;
	my %cookies = getAll();
	foreach (keys %cookies) {
		if ($_ eq $name) { 
			return $cookies{$_};
		} 
	}
	return;
}


# Get cookies;
sub getAll() {
	my(@rawCookies);
	if ($ENV{'HTTP_COOKIE'}) { @rawCookies = split (/; /,$ENV{'HTTP_COOKIE'}); }
	my(%cookies);
	foreach(@rawCookies){
	    my($key, $val) = split (/=/,$_);
	    $cookies{$key} = $val;
	} 
	return %cookies; 
}

