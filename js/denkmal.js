var visible_location=0;
var visible_audio_info=0;
var posx;
var posy;
var numInputWaits=0;
var searchingAnimVisible=0;
var links_actionshow=0;
var formupdate=0;


var IE;
var lista = '#ebebeb';
var listb = '#f8f8f8';
var listactive = '#96DD6B';
var searchboxtext = '';
var regex_email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

function denkmal_mousein(id, loc, box, dotsize, highlightdot, listactiveOverwrite) {
	// Highlight dot:
	if (highlightdot==1) {
		if (document.getElementById('dotimg_' + id).width>0) {
			document.getElementById('dotimg_' + id).width = dotsize+2;
			if (box==1) {
				var locationdot = document.getElementById('dot_' + id);
				moveLocation(getX(locationdot), getY(locationdot));
				document.getElementById('location').innerHTML='&nbsp;' + loc + '&nbsp;';
				document.getElementById('location').style.visibility='visible';
			}
		}
	}
	// Highlight event in list:
	var eventlocid = 'event_loc_' + id;
	if (highlightdot==0) { eventlocid = 'event_loc_tmp_' + id; }
	if (eventli = document.getElementById(eventlocid)) {
		if (listactiveOverwrite) {
			eventli.style.backgroundColor = listactiveOverwrite;
		} else {
			eventli.style.backgroundColor = listactive;
		}
	}
}

function denkmal_mousein_audio(text) {
	if (document.getElementById('audio_info') != null) {
		moveAudio(posx, posy);
		document.getElementById('audio_info').innerHTML='&nbsp;' + text + '&nbsp;';
		document.getElementById('audio_info').style.display='block';
		document.getElementById('audio_info').style.visibility='visible';
		visible_audio_info=1;
	}
}



function denkmal_mouseout(id, dotsize, highlightdot) {
	// Highlight dot:
	if (highlightdot) {
		if (document.getElementById('dotimg_' + id).width>0) {
			document.getElementById('location').style.visibility='hidden';
			document.getElementById('dotimg_' + id).width = dotsize;
		}
	}
	// Highlight event in list:
	var eventlocid = 'event_loc_' + id;
	if (highlightdot==0) { eventlocid = 'event_loc_tmp_' + id; }
	if (eventli = document.getElementById(eventlocid)) {
		if (eventli.className == 'a') {
			eventli.style.backgroundColor = lista;
		} else {
			eventli.style.backgroundColor = listb;
		}
	}
}

function denkmal_mouseout_audio() {
	if (document.getElementById('audio_info') != null) {
		document.getElementById('audio_info').style.visibility='hidden';
		document.getElementById('audio_info').style.display='none';
		visible_audio_info=0;
	}
}



function mousemove(e) {
	// Get mouse-pos:
	if (IE) { // grab the x-y pos.s if browser is IE
		posx = event.clientX + document.body.scrollLeft;
		posy = event.clientY + document.body.scrollTop;
	} else {  // grab the x-y pos.s if browser is NS
		posx = e.pageX;
		posy = e.pageY;
	}  
	if (posx < 0){posx = 0;}
	if (posy < 0){posy = 0;}  

	if (visible_location==1) { moveLocation(posx, posy); }
	if (visible_audio_info==1) { moveAudio(posx, posy); }
}

function moveLocation(x,y) {
	document.getElementById('location').style.left = (x+12) + 'px';
	document.getElementById('location').style.top = y + 'px';
}
function moveAudio(x,y) {
	document.getElementById('audio_info').style.left=(x+10) + 'px';
	document.getElementById('audio_info').style.top=(y+10) + 'px';
}






function searchBoxAction(field, nowait) {
	val = field.value;
	if (val != searchboxtext) {
		searchboxtext = val;
		if (val == '') {
			searchClear();
		} else {
			numInputWaits++;
			if (searchingAnimVisible != 1) { searchingAnim("searching_96dd6b.gif", "Suche läuft..."); }
			if (nowait) {
				inputWaitEnd();
			} else {
				setTimeout("inputWaitEnd();",300);
			}
		}
	}
}

function inputWaitEnd() {
	numInputWaits--;
	if (numInputWaits==0) {
		if (document.getElementById('q').value == '') {
			searchClear();
		} else {
			search(searchboxtext);
		}
	}
}
function searchClear() {
	if (document.getElementById('q')) { document.getElementById('q').value=''; }
	document.getElementById('search_results').innerHTML = '';
	document.getElementById('search_results').style.visibility = 'hidden';
	searchingAnimVisible=0;
	searchboxtext = '';
}
function searchingAnim(wheelpic, text) {
		document.getElementById('search_results').innerHTML = "<img src='/pics/"+wheelpic+"' id='searching'> <h3 style='display:inline;'>"+text+"</h3>";
		document.getElementById('search_results').style.visibility = 'visible';
		searchingAnimVisible=1;
}


function searchText(str) {
	var field = document.getElementById('q');
	field.value = str;
	searchBoxAction(field, 1);
}

function search(str) {
	loadXMLDoc("/?action=search&q="+ encodeURIComponent(str), searchResults);
	pageTracker._trackPageview("/search?q="+str);
}

function searchMore(str, time, mode) {
	loadXMLDoc("/?action=search&q="+encodeURIComponent(str)+"&qtime="+encodeURIComponent(time)+"&qmode="+encodeURIComponent(mode), searchResults);
}

function searchResults() {
    if (req.readyState == 4) {
        if (req.status == 200) {
			if (document.getElementById('q')) { if (document.getElementById('q').value == '') { return; } }
			document.getElementById('search_results').innerHTML = req.responseText;
			searchingAnimVisible=0;
        } else {
			searchClear();
		}
		if (formupdate == 1) { formChange(1); }
	}
}





// Get position of an element:
function getX(el) {
	var offset = 0;
	do { offset += el['offsetLeft'] || 0; el = el.offsetParent; } while(el);
	return offset;
}
function getY(el) {
	var offset = 0;
	do { offset += el['offsetTop'] || 0; el = el.offsetParent; } while(el);
	return offset;
}


function links_hide() {
	links_actionshow = 0;
	setTimeout("links_maybeshow()", 100);
}
function links_show() {
	links_actionshow = 1;
	setTimeout("links_maybeshow()", 10);
}
function links_maybeshow() {
	var w = document.getElementById('links_cont');
	if (links_actionshow==1) {
		searchClear();
		w.style.display='block';
	} else {
		w.style.display='none';
	}
}

function wettbewerb_hide() {
	if (document.getElementById('wettbewerb')) {
		var w = document.getElementById('wettbewerb');
		w.style.display='none';
		document.getElementById('wettbewerb_error').innerHTML='';
	}
}
function wettbewerb_show() {
	if (document.getElementById('wettbewerb')) {
		var w = document.getElementById('wettbewerb');
		w.style.display='block';
	}
}

function wettbewerb_toggle() {
	if (document.getElementById('wettbewerb')) {
		var w = document.getElementById('wettbewerb');
		if (w.style.display=='block') {
			wettbewerb_hide();
		} else {
			wettbewerb_show();
		}
	}
}

function wettbewerb_submit() {
	if (document.getElementById('wettbewerb')) {
		var wid=document.getElementById('wettbewerb_id').value;
		var wname=document.getElementById('wettbewerb_name').value;
		var wemail=document.getElementById('wettbewerb_email').value;
		loadXMLDoc("/?action=wettbewerb_submit&wname="+wname+"&wemail="+wemail+"&wid="+wid, wettbewerbResult);
		document.getElementById('mitmachen').disabled=true;
	}
}

function wettbewerbResult() {
    if (req.readyState == 4) {
        if (req.status == 200) {
			var wi = document.getElementById('wettbewerb_inhalt');
			var status=req.responseText.substring(0,req.responseText.indexOf("\n"));
			var html=req.responseText.substring(req.responseText.indexOf("\n")+1);
			document.getElementById('mitmachen').disabled=false;
			if (status=='ERROR') {
				document.getElementById('wettbewerb_error').innerHTML=html;
			} else if (status=='OK') {
				document.getElementById('wettbewerb_error').innerHTML='';
				document.getElementById('wettbewerb_inhalt').innerHTML=html;
			}
        }
    }
}



var mail1="kontakt@";
function mailus() {
	location.href="mailto:" + mail1 + "denkmal.org";
}

function rechtliches() {
	alert('Die angebotenen Songs sind Hörproben.\nVon kaufbarer Musik werden nur Previews angeboten.\nSollten Sie mit der Veröffentlichung eines Songs nicht einverstanden sein, so melden Sie das bitte an: kontakt@denkmal.org.\n\nAchtung: Es besteht keine Garantie, dass an einem Event die hier angebotene Musik läuft. ;)')
}