// AJAX-API Script based on apple ajax-howto


var req;

function loadXMLDoc(url, func) {
	req = false;
    // branch for native XMLHttpRequest object
    if(window.XMLHttpRequest) {
    	try {
			req = new XMLHttpRequest();
        } catch(e) {
			req = false;
        }
    // branch for IE/Windows ActiveX version
    } else if(window.ActiveXObject) {
       	try {
        	req = new ActiveXObject("Msxml2.XMLHTTP");
      	} catch(e) {
        	try {
          		req = new ActiveXObject("Microsoft.XMLHTTP");
        	} catch(e) {
          		req = false;
        	}
		}
		//Avoid caching in IE:
		url = url+getNocacheStr();
    }
	
	if(req) {
		req.onreadystatechange = func;
		req.open("GET", url, true);
		req.send("");
	}
}


// Get a short string by time so the browser doesn't cache
function getNocacheStr() {
	var d = new Date();
	var time = d.getTime();
	return "&nocache=" +time;
}

