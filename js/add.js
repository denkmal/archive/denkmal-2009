var numInputWaits_form=0;
var previewUrl = '';


// Location was changed in the dropdown field:
function locationChg() {
	if (getSelectText('wo') == '-') {
		// Show fields:
		document.getElementById('wo_name').disabled=false;
		document.getElementById('wo_ort').disabled=false;
		document.getElementById('wo_website').disabled=false;
		document.getElementById('newlocation').style.display='block';
		document.getElementById('info_website').innerHTML='';
		// Hide events for location:
		searchClear();
		formChange();
	} else {
		// Hide fields:
		document.getElementById('wo_name').disabled=true;
		document.getElementById('wo_ort').disabled=true;
		document.getElementById('wo_website').disabled=true;
		document.getElementById('newlocation').style.display='none';
		var website = document.add_event.wo.options[document.add_event.wo.selectedIndex].id;
		document.getElementById('info_website').innerHTML='<a target=\"_blank\" href=\"'+website+'\">'+website+'</a>';
		formupdate = 1;
		//Search for events at this location:
		searchingAnimLoc();
		searchMore(getSelectText('wo'), 'future', 'location');
	}
}


// Search for events on page load (if a location is already picked)
function searchEvtsOnload() {
	if (document.getElementById('wo').value != '-') {
		formupdate = 1;
		searchingAnimLoc();
		searchMore(getSelectText('wo'), 'future', 'location');
	} else {
		formChange();
	}
}

function getSelectText(id) {
	return document.getElementById(id).options[document.getElementById(id).selectedIndex].text;
}

function searchingAnimLoc() {
	searchingAnim("searching_ffffff.gif", "Suche Events zu Location...");
}


function formChange(nowait) {
	formupdate = 0;
	var previewUrlNew = getPreviewUrl();
	if (previewUrlNew != previewUrl) {
		previewUrl = previewUrlNew;
		numInputWaits_form++;
		if (nowait == 1) {
			inputWaitEnd_form();
		} else {
			setTimeout("inputWaitEnd_form();",300);
		}
	}
}
function inputWaitEnd_form() {
	numInputWaits_form--;
	if (numInputWaits_form==0) {
		previewUpdate();
	}
}

function previewUpdate() {
	loadXMLDoc("/?action=eventpreview&"+previewUrl, previewResult);
}
function previewResult() {
    if (req.readyState == 4) {
        if (req.status == 200) {
			if (document.getElementById('preview')) {
				document.getElementById('preview').innerHTML = req.responseText;
			}
        }
    }
}

function getPreviewUrl() {
	var url = '';
	var beschreibung = document.getElementById('was').value;
	var datum = document.getElementById('wann_datum').value;
	var zeit = document.getElementById('wann_zeit1').value;
	var zeit2 = document.getElementById('wann_zeit2').value;
	var locname = '';
	var locurl = '';
	if (getSelectText('wo') == '-') {
		locname = document.getElementById('wo_name').value;
		locurl = document.getElementById('wo_website').value;
	} else {
		locname = getSelectText('wo');
		locurl = document.add_event.wo.options[document.add_event.wo.selectedIndex].id;
	}	
	
	url = 'datum='+datum+'&zeit='+zeit+'&zeit2='+zeit2+'&beschreibung='+encodeURIComponent(beschreibung)+'&locname='+encodeURIComponent(locname)+'&locurl='+encodeURIComponent(locurl);
	return url;
}