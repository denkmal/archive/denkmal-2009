#!/usr/bin/perl

use DBI;
use CGI;
#use CGI::Carp qw(fatalsToBrowser);

# HTML-Template:
my $htmlfile = "denkmal.org_add.html";
my $email = 'events@denkmal.org';

$query = CGI::new();
my $action = $query->param("action");
my $wo = $query->param("wo");
my $wo_name = $query->param("wo_name");
my $wo_ort = $query->param("wo_ort");
my $wo_website = $query->param("wo_website");
my $wann_datum = $query->param("wann_datum");
my $wann_zeit1 = $query->param("wann_zeit1");
my $wann_zeit2 = $query->param("wann_zeit2");
my $was = $query->param("was");
my $links = $query->param("links");
my $beta = $query->param("beta");
my $content, $formular, $fehlermeldung, $end, $pagetitle;

my $dbh = DBI->connect("DBI:mysql:denkmal_org:localhost","denkmal_org","thedog")  || die "Database error: $DBI::errstr";
$set = $Select2 = $dbh->prepare("SET NAMES utf8");
$set->execute; $set->finish;

my($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900; $Wochentag--;



$pagetitle = 'Event hinzufügen';

if ($action) {
	# Eingaben überprüfen:
	my $fertige_location=0;
	if ($wo ne "-") { $fertige_location=1; } else { $wo=''; }
	
	if ($fertige_location==0) {
		if ($wo_name eq "") { $fehlermeldung.="Der Name der neu eingetragenen Location ist unvollständig.<br>"; }
		if ($wo_ort eq "") { $fehlermeldung.="Der genaue Ort der neu eingetragenen Location ist unvollständig.<br>"; }
		if ($wo_website ne "" && $wo_website !~ /^http:\/\/./) { $fehlermeldung.="Die Webseite der neu eingetragenen Location ist unvollständig. Bitte im Format http://www.example.com angeben, oder das Feld leer lassen.<br>"; }
	}
	if ($wann_datum !~ /^\d{1,2}\.\d{1,2}\.\d{4}$/) { $fehlermeldung.="Die Datumsangabe ist ungültig. Bitte im Format DD.MM.YYYY angeben.<br>"; }
	if ($wann_zeit1 !~ /^\d{1,2}:\d{2}$/) { $fehlermeldung.="Die Zeitangabe ist ungültig. Bitte im Format HH:MM angeben.<br>"; }
	if ($wann_zeit2 ne "" && $wann_zeit2 !~ /^\d{1,2}:\d{2}$/) { $fehlermeldung.="Die End-Zeitangabe ist ungültig. Bitte im Format HH:MM angeben, oder das Feld leer lassen.<br>"; }
	if ($was eq "") { $fehlermeldung.="Die Beschreibung des Events ist unvollständig.<br>"; }
	if ($was =~ m/<a/i && $was =~ m/<\/a>/i) { $fehlermeldung.="Die Beschreibung des Events ist ungültig.<br>"; }
	$was =~ s/\r//g; $was =~ s/\n/ /g;	# Zeilenumbrüche löschen.
	
	# Sind die Eingaben vollständig?
	if ($fehlermeldung) {
		undef $action;
	} else {
		# Event eintragen:
		if ($fertige_location==0) { $wo=$wo_name; }
		$wann_datum =~ /^(\d{1,2})\.(\d{1,2})\.(\d{4})$/;
		if ($links ne '') { $was = "$was / $links"; }
		&addEvent($3,$2,$1, $wann_zeit1, $wann_zeit2, $wo, $was, '0', '1', '1');
		
		# Neue Location eintragen:
		if ($fertige_location==0) {
		
			# Apostroph für SQL-Query ersetzen:
			$wo_name =~ s/'/\\'/g;
			$wo_ort =~ s/'/\\'/g;
			$wo_website =~ s/'/\\'/g;
		
			$ins = $dbh->prepare("INSERT INTO locations (name,website,enabled) VALUES('$wo_name / $wo_ort','$wo_website','0')");
			$ins->execute;
			$ins->finish;
		}
		
		$content .= "Vielen Dank!<br><br>";
		$content .= "Ihre Eintragung wurde gespeichert und wird nun überprüft. Dies kann bis zu einem Tag dauern. Danach wird der Event, sofern er akzeptiert wurde, auf denkmal.org erscheinen.<br>";
		$content .= "<br>";
		$content .= "Sollten Sie Kommentare oder Fragen haben, so schreiben Sie doch ein E-Mail an <a href='mailto:$email'>$email</a> oder rufen Sie an oder schreiben Sie ein SMS an 079 / 367 78 97.<br>";
	}

}

if (!$action) {
	$content .= "Sie können mit diesem Formular einen kommenden Event in der Region Basel eintragen.<br>";
	$content .= "Jeder Eintrag wird <i>geprüft</i>, bevor er auf denkmal.org erscheint. Es kann deshalb durchaus einen Tag dauern, bis Ihr Vorschlag auf der Webseite erscheint.<br>";
	
	if ($fehlermeldung) { $content .= "<br><div style='color:red;'><b>Bitte überprüfen Sie Ihre Eingaben nochmals:</b><br>$fehlermeldung</div>"; }


	$formular .= "<form method='POST' name='add_event'>";
	$formular .= "<input type='hidden' name='action' value='add'>";

	$formular .= "<div class='subfield'>";
	$formular .= "<h3>Wo:</h3>";
	$formular .= "Eine Location aus der Liste wählen:<br>";
	$formular .= "<label>Location:</label> <select name='wo' id='wo' onchange='locationChg();'>";
	$formular .= "<option>-</option>";
	$Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null) and (enabled = '1') ORDER by name");
	$Select->execute;
	$val="";
	while ($Row=$Select->fetchrow_hashref) {
		if ($Row->{name} eq $wo) { $formular .= "<option id='$Row->{website}' SELECTED>$Row->{name}</option>"; $val=$Row->{website} }
		else { $formular .= "<option id='$Row->{website}'>$Row->{name}</option>"; }
	}
	$Select->finish();
	$end .= "<script type='text/javascript'>searchEvtsOnload();</script>";
	if ($val) { $formular .= "</select> <font id='info_website'><a href='$val' target='_blank'>$val</a></font><br>"; }
	else { $formular .= "</select> <font id='info_website'></font><br>"; }
	$formular .= "<div id='newlocation'";
	if ($wo) { $formular .= " style='display:none;'"; }
	$formular .= ">";
	$formular .= "<hr><u>Oder</u>: Eine noch nicht gespeicherte Location angeben:<br>";
	$formular .= "<label>Name:</label> <input type='text' name='wo_name' id='wo_name' value='$wo_name' onkeydown='formChange();' onkeyup='formChange();' onchange='formChange();' onpaste='formChange();' onmousedown='formChange();'> <span class='bsp'>(Bsp: 'Capri Bar')</span> <br>";
	$formular .= "<label>Ort:</label> <input type='text' name='wo_ort' id='wo_ort' value='$wo_ort'> <span class='bsp'>(Bsp: 'Klybeckstrasse 240b' oder 'Beim Florabeach')</span> <br>";
	if (!$fehlermeldung) { $val="http://"; } else { $val=$wo_website; }
	$formular .= "<label>Webseite:</label> <input type='text' name='wo_website' id='wo_website' value='$val' onkeydown='formChange();' onkeyup='formChange();' onchange='formChange();' onpaste='formChange();' onmousedown='formChange();'> <span class='bsp'>(Bsp: 'http://www.asdf.com/' oder '')</span> <br>";
	$formular .= "</div>";
	$formular .= "</div>";

	$formular .= "<div class='subfield'>";
	$formular .= "<h3>Wann:</h3>";
	if (!$fehlermeldung) { $val="$Monatstag.$Monat.$Jahr"; } else { $val=$wann_datum; }
	$formular .= "<label>Datum:</label> <input type='text' name='wann_datum' id='wann_datum' value='$val' onkeydown='formChange();' onkeyup='formChange();' onchange='formChange();' onpaste='formChange();' onmousedown='formChange();'> <span class='bsp'>(Bsp: '17.7.$Jahr')</span> <br>";
	if (!$fehlermeldung) { $val="22:00"; } else { $val=$wann_zeit1; }
	$formular .= "<label>Zeit:</label> <input type='text' name='wann_zeit1' id='wann_zeit1' value='$val' onkeydown='formChange();' onkeyup='formChange();' onchange='formChange();' onpaste='formChange();' onmousedown='formChange();'> <span class='bsp'>(Bsp: '22:00')</span> <br>";
	if (!$fehlermeldung) { $val=""; } else { $val=$wann_zeit2; }
	$formular .= "<label>Ende (optional):</label> <input type='text' name='wann_zeit2' id='wann_zeit2' value='$val' onkeydown='formChange();' onkeyup='formChange();' onchange='formChange();' onpaste='formChange();' onmousedown='formChange();'> <span class='bsp'>(Bsp: '3:00')</span> <br>";
	$formular .= "</div>";

	$formular .= "<div class='subfield'>";
	$formular .= "<h3>Was:</h3>";
	if (!$fehlermeldung) { $val=""; } else { $val=$was; }
	$formular .= "<label>Beschreibung:</label> <textarea id='was' name='was' rows='3' cols='30' onkeydown='formChange();' onkeyup='formChange();' onchange='formChange();' onpaste='formChange();' onmousedown='formChange();'>$val</textarea><br>";
	$formular .= "<div style='padding-left:130px;'><span class='bsp'>(Bsp: 'Keiths Geburtstagsparty. Livemusik von The Examples danach DJs.')</span></div>";
	if (!$fehlermeldung) { $val=""; } else { $val=$links; }
	$formular .= "<label>Webseiten:</label> <textarea name='links' rows='3' cols='30'>$val</textarea><br>";
	$formular .= "<div style='padding-left:130px;'><span class='bsp'>(Webseiten von Bands, DJs oder Festivals zum Event.<br/>Diese Links werden gespeichert. Also nur einmal angeben!)</span></div>";
	$formular .= "</div>";
	
	$formular .= "<div class='subfield'>";
	$formular .= "<h3>Vorschau:</h3>";
	$formular .= "<div id='preview'>";
	$formular .= "</div>";
	$formular .= "</div>";

	$formular .= "<div class='subfield' style='text-align: right; border:0;'>";
	$formular .= "<input type='submit' value='Abschicken'>";
	$formular .= "</div>";
	

	$formular .= "</form>";

}




$dbh->disconnect();

# Das ganze im Template ausgeben:
if ($pagetitle ne '') { $pagetitle = " - $pagetitle"; }

open(HTML,$htmlfile);@html=<HTML>;
close(HTML);
$i=0;
foreach(@html) {
 $html[$i]=~s/\[TITLE\]/Einen Event hinzufügen/;
 $html[$i]=~s/\[CONTENT\]/$content/g;
 $html[$i]=~s/\[FORMULAR\]/$formular/g;
 $html[$i]=~s/\[IMGLOAD\]//g;
 $html[$i]=~s/\[END\]/$end/g;
 $html[$i]=~s/\[PAGETITLE\]/$pagetitle/g;
 $i++;
}
print "Content-type: text/html\n\n";
print @html;









## Einen Event in der Datebank eintragen
sub addEvent() {
	my($y,$m,$d, $zeit1, $zeit2, $wo, $was, $enabled, $nodeletion, $star) = @_;
	
	# Apostroph für SQL-Query ersetzen:
	$was =~ s/'/\\'/g;
	$wo =~ s/'/\\'/g;
	
	# Überprüfen, ob ein Alias für die Location besteht:
	$Select2 = $dbh->prepare("SELECT * FROM locations WHERE (name = '$wo') and (alias is not null) and (enabled = '1')");
	$Select2->execute; 
	$Row2=$Select2->fetchrow_hashref;
	if ($Row2) { $wo = $Row2->{alias}; }
	$Select2->finish();
	
	
	# Eintrag hinzufügen:
	$zeit1 = zeit_sql($zeit1);
	$zeit2 = zeit_sql($zeit2);
	if ($zeit2) {
		$ins = $dbh->prepare("INSERT INTO events (datum, zeit, zeit2, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$zeit2','$wo','$was','$enabled','$nodeletion','$star')");
	} else {
		$ins = $dbh->prepare("INSERT INTO events (datum, zeit, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$wo','$was','$enabled','$nodeletion','$star')");
	}
	$ins->execute;
	$ins->finish;
}








## Zeit formatieren:
sub zeit() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2/g;
 return $zeit;
}

## Zeit nach SQL formatieren:
sub zeit_sql() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2:00/g;
 return $zeit;
}



## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
 my($y, $m, $d) = @_;
  if ($m < 3) { $f = -1; } else { $f = 0; }
  return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
}





	