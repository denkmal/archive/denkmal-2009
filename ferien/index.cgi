#!/usr/bin/perl

use CGI;
use Date::Calc qw(Date_to_Days);
use strict;

my @date_ziel = (2009,8,10);
my $query = CGI::new();




# Anzahl tage berechnen:
my($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900; $Wochentag--;
my @date_now = ($Jahr,$Monat,$Monatstag);
my $tage = Date_to_Days(@date_ziel) - Date_to_Days(@date_now);
if ($tage<0) { $tage=0; }

# Widget request
if ($query->param("w")) {
	print "Content-type: text/javascript\n\n";
	exit 0;
}


print "Content-Type: text/html\n\n";

print <<JOO;
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="DENKMAL.ORG - Basels Eventkalender. WaslOift am Rheinknie und wie hört es sich an?"/>
<meta name="keywords" content="denkmal, basel, bs, denkmal.org, eventkalender, events, party, parties, ausgang, kultur, wasloift"/>
<title>DENKMAL.ORG im Sommerloch</title>
<style>
body {
	margin: 0;
	background-color: #96DD6B;
	font-family: "Trebuchet MS", Trebuchet, Helvetica, Arial, Verdana, sans-serif;
}
#tage {
	position: absolute;
	top: 175px;
	left: 0px;
	width: 460px;
	font-size: 5em;
	font-weight: 800;
	text-align: center;
}
</style>
</head>
<body>

<table width=100% height=100%><tr height=100%><td valign="middle" align="center" height=100%>
<div style='position: relative; width: 500px; height: 436px;'>
JOO

print "<img src='/ferien/loch.jpg' alt='Tage bis zum Alltag:'>";
print "<div id='tage'>$tage</div>";

print <<JOO;
</div>
</td></tr></table>

<!-- Start of Google Analytics Code -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-570541-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- End of Google Analytics Code -->

</body>
</html>
JOO
