#!/usr/bin/perl

use CGI;
#use CGI::Carp qw(fatalsToBrowser);
use DBI;
use CGI;
use LWP::UserAgent;
use Encode;
use POSIX;
use Cookie;
use strict;


# Wann beginnt der neue Tag:
my $morninghour = 6;
# HTML-Template:
my $htmlfile = "denkmal.org.html";
my $htmlfile_mobile = "denkmal.org_mobile.html";
my $htmlfile_iphone = "denkmal.org_iphone.html";
my $htmlfile_widget = "denkmal.org_widget.html";
my $htmlsrc;
# Relativer Pfad vom Client zu den Bildern:
my $picsdir = "/pics";
# Relativer Pfad vom Client zu den Audio Dateien:
my $audiodir = "/audio";

my $link_prefix = "";


my $query = CGI::new();
my $taggesucht = $query->param("tag");
my $showall = $query->param("showall");
my $action = $query->param("action");
my $q = $query->param("q");
my $qmode = $query->param("qmode");
my $qtime = $query->param("qtime");
my $beta = $query->param("beta");
	if ($showall!=1) { $showall=0; }
my ($dots, $list, $datumevents, $navtage, $showall_link, $soundofthemoment, $wettbewerb, $wettbewerb_inhalt, $links, $pagetitle, $showmore);
my $list_alternate=0;
my ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag);


 # MOBILE DEVICE?
 our $mobile=0;		# Showing mobile-enabled version?
 my $mobile_param = $query->param("m");
 my $mobileq;		# URL-Param
 if (isMobile($ENV{HTTP_USER_AGENT})) { $mobile=1; }
 if ($mobile>0 && $mobile_param<0) { $mobileq=-1; } elsif ($mobile==0 && $mobile_param>0) { $mobileq=1; }
 my $mobileString;
 if ($mobileq) { $mobileString="&m=$mobileq"; }
 if ($mobile_param>0) { $mobile=1; } elsif ($mobile_param<0) { $mobile=0; }
 if ($q) { $mobile=0; }
 our $iphone=0;		# Showing iphone-enabled version?
 if ($mobile) {
  if (isIphone($ENV{HTTP_USER_AGENT})) { $iphone=1; }
 }
 
 # WIDGET?
 my $widget = 0;
 if ($query->param("w")) {
	$widget = 1;
	$link_prefix = "http://www.denkmal.org";
	$taggesucht = '';
 }

 # DATENBANK
 my $dbh = DBI->connect("DBI:mysql:denkmal_org:localhost","denkmal_org","thedog")  || die "Database error: $DBI::errstr";
 my $set = $dbh->prepare("SET NAMES utf8");
 $set->execute; $set->finish;

 # DATUMZEUGS und gesuchter Tag suchen:
 my ($datumgesucht, $datumgesucht_sql);
 my @tage = ("So","Mo","Di","Mi","Do","Fr","Sa");
 my @monate_kurz = ("Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez");
 &getDatum();

 # Links (URLS):
 my (@url_Name, @url_Url, %NameToOnlyifmarked);

 # Dots:
 my %PrintedLocId;
 my $NoLocCounter;




if ($action eq 'search') {
	# SEARCH
	# Get urls to link to:
	getUrls();
	# Set id-counter for locations high enough:
	$NoLocCounter=1000;
	# Get matching events:
	my $searchresults .= "<div class='events'>" .listEventsByQuery($q, $qmode, $qtime). "</div>";
	$dbh->disconnect();
	# Print page:
	print "Content-type: text/html; charset=utf-8\n\n";
	print $searchresults;
	# End script:
	exit;
	
} elsif ($action eq 'eventpreview') {
	# EVENT PREVIEW
	# Get urls to link to:
	getUrls();
	# Set id-counter for locations high enough:
	$NoLocCounter=5000;
	# Get preview
	my $event;
		if ($query->param("datum") =~ /^(\d{1,2})\.(\d{1,2})\.(\d{4})$/) { $event->{datum} = "$3-$2-$1"; }
		if ($query->param("zeit") =~ /^(\d{1,2}):(\d{1,2})$/) { $event->{zeit} = "$1:$2"; }
		if ($query->param("zeit2") =~ /^(\d{1,2}):(\d{1,2})$/) { $event->{zeit2} = "$1:$2"; }
		$event->{beschreibung} = $query->param("beschreibung");
			if (length($event->{beschreibung}) > 150) { $event->{beschreibung} = substr($event->{beschreibung}, 0, 150); }
			$event->{beschreibung} =~ s/</&lt;/g;
			$event->{beschreibung} =~ s/</&gt;/g;
	my $location;
		$location->{name} = $query->param("locname");
			if (length($location->{name}) > 50) { $location->{name} = substr($location->{name}, 0, 50); }
			$location->{name} =~ s/</&lt;/g;
			$location->{name} =~ s/</&gt;/g;
		if ($query->param("locurl") =~ /^(https?:\/\/.+)$/) { $location->{website} = "$1"; }
			$location->{website} =~ s/</&lt;/g;
			$location->{website} =~ s/</&gt;/g;
	my $preview = "<div class='events'><ul>" .getEvent($event, $location, 1, 0, 1). "</ul></div>";
	$dbh->disconnect();
	# Print page:
	print "Content-type: text/html; charset=utf-8\n\n";
	print $preview;
	# End script:
	exit;
}

# WETTBEWERB SUBMIT:
if ($action eq 'wettbewerb_submit') {
	my $error;
	# Check values:
	my $wid=$query->param("wid");
	my $wname=$query->param("wname");
	my $wemail=$query->param("wemail");
	if ($wid !~ /^\d+$/) { wettbewerb_error("Ungültige Wettbewerbs-ID"); }
	if (length($wname)<5) { wettbewerb_error("Für den Wettbewerb musst Du Deinen vollständigen Namen angeben!"); }
	if ($wemail !~ /^.+@[^\.].*\.[a-z]{2,}$/) { wettbewerb_error("Für den Wettbewerb musst Du Deine gültige E-Mail Adresse angeben!"); }

	# Check cookie:
	if (Cookie::get("wettbewerb_$wid")) { wettbewerb_error("Die Teilnahme am Wettbewerb ist pro Person nur einmal erlaubt! Du hast schon teilgenommen."); }

	# Check DB rows:
	my $Select = $dbh->prepare("SELECT * FROM wettbewerb_teilnehmer WHERE (wettbewerbid=$wid) AND (email='$wemail')");
	$Select->execute;
	if ($Select->rows > 0) { wettbewerb_error("Die Teilnahme am Wettbewerb ist pro Person nur einmal erlaubt! Diese E-Mail Adresse nimmt schon teil."); }
	$Select->finish;

	# Insert row:
	my $Insert = $dbh->prepare("INSERT INTO wettbewerb_teilnehmer (wettbewerbid,email,name,datum,zeit) VALUES($wid,'$wemail','$wname',CURDATE(),CURTIME())");
	$Insert->execute; $Insert->finish;

	# Print cookie:
	Cookie::set("wettbewerb_$wid", "$wemail");

	# Print response:
	print "Content-type: text/html; charset=utf-8\n\n";
	print "OK\n";
	print wettbewerb_done_str();

	# End script:
	$dbh->disconnect();
	exit;
}
sub wettbewerb_done_str() {
	my $str='';
	$str .= "Vielen Dank fürs Mitmachen!<br/>";
	$str .= "<br/>";
	$str .= "Die Gewinner werden per E-Mail benachrichtigt.<br/>";
	$str .= "<input type='button' value='schliessen' class='button' id='schliessen' onclick='wettbewerb_hide();'/>";
	return $str;
}
sub wettbewerb_error($) {
	my($msg) = @_;
	print "Content-type: text/html; charset=utf-8\n\n";
	print "ERROR\n";
	print $msg. "<br/><br/>";
	# End script:
	$dbh->disconnect();
	exit;
}


 
# CACHING:
my $cache_identifier = "wasloift_" .$datumgesucht_sql. "_" .$showall. "_" .$mobile. "_" .$iphone. "_" .$widget;
my $cache = getCache($cache_identifier);

if ($cache && !$beta) {
	$htmlsrc = $cache;

} else {
	# NO CACHE - So do the whole thing:
	 # Get urls to link to:
	getUrls();

	my $wettbewerb_link='';
	my $Wett;
	if ($mobile==0) {
		my $Select = $dbh->prepare("SELECT * FROM wettbewerb WHERE (enabled=1)");
		$Select->execute;
		if ($Select->rows > 0) {
			$Wett=$Select->fetchrow_hashref;
			$wettbewerb_link = "<a href='javascript:;' onClick='wettbewerb_toggle();' id='wettbewerb_link'>" .$Wett->{linktext}. "</a>";
			$wettbewerb .=  "<div id='wettbewerb'>";
			$wettbewerb .=  "<div id='wettbewerb_text'>";
			$wettbewerb .= add_links($Wett->{text});
			$wettbewerb .= "</div>";
			$wettbewerb .= "<div id='wettbewerb_error'>";
			$wettbewerb .= "</div>";
			$wettbewerb .= "<div id='wettbewerb_inhalt'>";
			$wettbewerb .= "[WETTBEWERB_INHALT]";
			$wettbewerb .= "</div>";
			$wettbewerb .= "</div>";
		}
	}


	# Sound of the Moment:
	my $Select = $dbh->prepare("SELECT * FROM events WHERE (ort='denkmal_sm' AND id=-100 AND enabled=1)");
	$Select->execute;
	my $has_sm=0;
	if ($Select->rows > 0) {
		my $Row=$Select->fetchrow_hashref;
		if ($Row->{star}) {
			my $soundofthemoment_text = $Row->{beschreibung};
			# Add Wettbewerb link:
			if ($Wett) {
				$soundofthemoment_text .= "<br/>" .$wettbewerb_link;
			}
			# Count newlines:
			my $numnewlines=0; while ($soundofthemoment_text =~ /<BR\/?>/ig) { $numnewlines++; }
			# Show song:
			$soundofthemoment .= "<div id='sm_wimpy'>" .wimpyCode("$audiodir/".$Row->{audio}, "denkmal_sm", "f8d758"). "</div>";
			$soundofthemoment .= "<div id='sm_text' style='top:" .(24-($numnewlines*5)). "px;'>" .add_links($soundofthemoment_text). "</div>";
			$has_sm=1;
		}
	}
	if (!$has_sm) {
		# No sound of the moment:
		$soundofthemoment = "<div id='sm_text' style='top:19px;left:7px;'>Kein Sound of the Moment<br/>Hast Du einen <a href='mailto:kontakt\@denkmal.org?subject=Sound%20of%20the%20Moment'>Vorschlag</a>?</div>";
	}


	 # Datum:
	 $datumevents="<div id='datum'>Events in Basel für<br/>$taggesucht, $datumgesucht</div>";


	# Web-Links
	if ($mobile==0 || $iphone==1) {
		my $Select = $dbh->prepare("SELECT * FROM link ORDER BY url");
		$Select->execute;
		while (my $link=$Select->fetchrow_hashref) {
			my $domain = $link->{url};
			$domain =~ s/^https?:\/\///i;
			$domain =~ s/^www\.//i;
			$domain =~ s/\/.*$//;
			
			$links .= '<li><a href="' .$link->{url}. '" target="_blank">';
			$links .= '<span class="url">' .$domain. '</span>';
			if ($link->{description}) { $links .= ' <small>' .$link->{description}. '</small>'; }
			$links .= '</a></li>';
		}
	}



	# List of events on this day:
	$list .= listEvents();
	if ($list eq '') { $list = 'Keine events...'; }
	# Dots of locations with no event:
	$dots .= getLocationDots();


	# Widget:
	if ($widget) {
		my @events = split(/<\/li>\n*<li/, $list);
		my $maxEvents = 5;
		if ($#events+1 <= 6) {
			$maxEvents = 6;
		}
		$list = join("</li><li", splice(@events,0,$maxEvents));
		my $moreEvents = $#events + 1;
		if ($moreEvents>0) {
			$showmore = "<div id='showmore'><a href='http://www.denkmal.org/' target='_blank'>" .$moreEvents. " weitere Events...</a></div>";
		}
	}


	if ($widget) {
			$htmlfile = $htmlfile_widget;
			$list =~ s/"/\\"/g;
			$list =~ s/\n//g;
	} elsif ($mobile) { 
		if ($iphone) {
			$htmlfile = $htmlfile_iphone;
		} else {
			$htmlfile = $htmlfile_mobile;
		}
	}
	
	open(HTML,$htmlfile); 
		my @htmls=<HTML>;
	close(HTML);
	$htmlsrc = join('',@htmls);
	$htmlsrc=~s/\[DOTS\]/$dots/g;
	$htmlsrc=~s/\[LIST\]/<ul>$list<\/ul>/g;
	$htmlsrc=~s/\[DATUMEVENTS\]/$datumevents/g;
	$htmlsrc=~s/\[SOUNDOFTHEMOMENT\]/$soundofthemoment/g;
	$htmlsrc=~s/\[WETTBEWERB\]/$wettbewerb/g;
	$htmlsrc=~s/\[LINKS\]/$links/g;
	$htmlsrc=~s/\[SHOWMORE\]/$showmore/g;

	# Set cache:
	if (!$beta) { setCache($cache_identifier, $htmlsrc); }
}


# Links zu anderen Wochentagen:
	 my $n=$Wochentag;
	 my $i=0;
	 my $x=1; my $w=25;
	 if ($iphone) {
		$navtage.="<ul id='tage'>";
	 }
	 foreach(@tage) {
	  if($mobile){
	   if ($iphone) {
	    my $classes = '';
		if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { $classes.=' active'; }
		if ($i==$#tage) { $classes.=' last'; }
		$navtage.="<li class='$classes'>";
	   } else {
		if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { $navtage.="<b><u>"; }
	   }
	  } else {
		$navtage.="<div class='navtag' style='left:" .$x. "px;'>";
	  }

	  my $url="?tag=$tage[$n]";
	   if ($mobileString) { $url.=$mobileString; }
	   if ($showall==1) { $url.="&showall=1"; }
	  my $classes=''; if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { $classes.=' active'; }
	  $navtage .= "<a href=\"$url\" class='$classes'>$tage[$n]</a>";

	  if($mobile){
	   if ($iphone) {
	    $navtage.="</li>";
	   } else {
 	    if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { $navtage.="</b></u>"; }
		$navtage.=" ";
	   }
	  } else {
		$navtage.="</div>";
	  }
	  $x+=$w;
	  $n++; if($n>6) {$n=0;} 
	  $i++;
	 }
	 if ($iphone) {
		$navtage.="</ul>";
	 }
	 
	$htmlsrc=~s/\[NAVTAGE\]/$navtage/g;
	
	$htmlsrc=~s/\[MOBILE\]/$mobileString/g;
	
	
	# Showall label:
	 if ($mobile==0) {
		if ($showall==1) { $showall_link.= "<a href='?tag=$taggesucht$mobileString'>Weniger anzeigen</a>"; }
		else { $showall_link.= "<a href='?tag=$taggesucht&showall=1$mobileString'>Alle anzeigen</a>"; }
	 }
	$htmlsrc=~s/\[SHOWALL\]/$showall_link/g;




# DYNAMIC STUFF (not cached)
# Wettbewerb:
	my $Wett;
	if ($mobile==0) {
		my $Select = $dbh->prepare("SELECT * FROM wettbewerb WHERE (enabled=1)");
		$Select->execute;
		if ($Select->rows > 0) {
			$Wett=$Select->fetchrow_hashref;
			if (Cookie::get("wettbewerb_$Wett->{id}")) {
				$wettbewerb_inhalt .= wettbewerb_done_str();
			} else {
				$wettbewerb_inhalt .= "Dein voller Name:<br/>";
				$wettbewerb_inhalt .= "<input type='text' class='text' id='wettbewerb_name'/><br/>";
				$wettbewerb_inhalt .= "Deine E-Mail Adresse:<br/>";
				$wettbewerb_inhalt .= "<input type='text' class='text' id='wettbewerb_email'/><br/>";
				$wettbewerb_inhalt .= "<input type='hidden' id='wettbewerb_id' value='$Wett->{id}'/>";
				$wettbewerb_inhalt .= "Die Gewinner werden per E-Mail benachrichtigt.<br/>";
				$wettbewerb_inhalt .= "<table><tr>";
				$wettbewerb_inhalt .= "<td><input type='button' value='« abbrechen' class='button' id='abbrechen' onclick='wettbewerb_hide();'/></td>";
				$wettbewerb_inhalt .= "<td><input type='button' value='Mitmachen! »' class='button' id='mitmachen' onclick='wettbewerb_submit()';/></td>";
				$wettbewerb_inhalt .= "</tr></table>";
			}
		}
	}
	$htmlsrc=~s/\[WETTBEWERB_INHALT\]/$wettbewerb_inhalt/g;


	# Title
	if ($pagetitle ne '') { $pagetitle = " - $pagetitle"; }
	$htmlsrc=~s/\[PAGETITLE\]/$pagetitle/g;
 

# Statistics:
doStats();

$dbh->disconnect();

# Print page:
if ($widget) {
	print "Content-type: text/javascript\n\n";
} else {
	print "Content-type: text/html\n\n";
}
print $htmlsrc;




#########################################################################################################


# Get cache-html by identifier:
sub getCache($) {
	my($ident) = @_;
	my $Select = $dbh->prepare("SELECT * FROM cache WHERE (name='$ident')");
	$Select->execute; 
	my $cache=$Select->fetchrow_hashref;
	$Select->finish;
	if ($cache) {
		return $cache->{html};
	} else {
		return;
	}
}

# Set cache-html by identifier:
sub setCache($$) {
	my($ident, $html) = @_;
	# Check if we already have a row for this:
	my $Select = $dbh->prepare("SELECT * FROM cache WHERE (name='$ident')");
	$Select->execute; 
	my $cache=$Select->fetchrow_hashref;
	$Select->finish;
	my $ins;
	if ($cache) {
		$ins = $dbh->prepare("UPDATE cache SET html='" .escapeSql($html). "' WHERE (name = '$ident')");
	} else {
		$ins = $dbh->prepare("INSERT INTO cache (name,html) VALUES('$ident','" .escapeSql($html). "')");
	}
	$ins->execute;
	$ins->finish;
}



# Save user-statistics to db:
sub doStats() {
	# Mobile:
	if ($mobile) {
		if ($iphone) {
			my $edit = $dbh->prepare("UPDATE stats SET value=value+1 WHERE name='hits_iphone'");
			$edit->execute; $edit->finish;
		} else {
			my $edit = $dbh->prepare("UPDATE stats SET value=value+1 WHERE name='hits_mobile'");
			$edit->execute; $edit->finish;
		}
	}
}





sub getEventById($$$) {
	my($id, $showDatum, $popupMode) = @_;
	# Get event from DB:
	my $Select = $dbh->prepare("SELECT * FROM events WHERE id=$id");
	$Select->execute; 
	my $Event=$Select->fetchrow_hashref;
	$Select->finish;

	return getEventByEvent($Event, $showDatum, $popupMode);
}
sub getEventByEvent($$$) {
	my($Event, $showDatum, $popupMode) = @_;
	# Get location from DB:
	my $Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null) AND (enabled = '1') AND (name = '" .escapeSql($Event->{ort}). "')");
	$Select->execute;
	my $Location=$Select->fetchrow_hashref;
	$Select->finish;

	return getEvent($Event, $Location, $showDatum, 1, $popupMode);
}

sub getEvent($$$$$) {
	my ($Event, $Location, $showDatum, $doMouseover, $popupMode) = @_;
	my $str;

	# Eigenschaften:
	my $wimpyColor="69d74c";
	if ($Event->{star}) { $wimpyColor="f8d758"; }

	my $location_name;
	my $location_name_linked;
	if ($Location) {
		$location_name = $Location->{name};
		$location_name_linked = "<span class='list'>$Location->{name}</span>";
		if ($Location->{website}) { $location_name_linked="<a href='$Location->{website}' target='_blank' class='list' title='$Location->{website}'>$Location->{name}<\/a>"; } 
	} else {
		$location_name = $Event->{ort};
		$location_name_linked = "<span class='list'>$Event->{ort}</span>";
	}

	my $event_zeit = "";
	if ($Event->{zeit}) { $event_zeit=zeitformat($Event->{zeit},$Event->{zeit2})." "; }

	my $event_beschreibung = $Event->{beschreibung};
	$event_beschreibung = add_links($event_beschreibung);


	# Dot-image:
	my $dot="dot1.gif"; my $dotsize=7;
	if (!$Location) { $dot="dot0.gif"; }
	if ($Event->{star}) { $dot="dot3.gif"; $dotsize=9; }
	# Escape location name:
	my $location_name_ = $location_name;
	$location_name_=~s/'/\\'/g;
	# Dot-size:
	my $dotsize=7;
	if ($Event->{star}) { $dotsize=9; }

	# Add dot, if we do mouseover:
	if ($doMouseover && $Location) {
		# Dot html:
		my $zindex = 'z-index:2;';
		if ($Event->{star}) { $zindex = 'z-index:3;'; }
		$dots .= "<div id='dot_$Location->{id}' class='dot' style='left:$Location->{posx}px; top:$Location->{posy}px; $zindex'><a ";
		if ($Location->{website}) { $dots .= "target='_blank' href='$Location->{website}'"; } else { $dots .= "href='javascript:;'"; }
		$dots .= " onmouseover=\"denkmal_mousein('$Location->{id}','$location_name_',1,$dotsize,1);\" onmouseout=\"denkmal_mouseout('$Location->{id}',$dotsize,1);\"><img src=\"$link_prefix$picsdir/$dot\" id=\"dotimg_$Location->{id}\"/></a></div>";
		$PrintedLocId{$Location->{id}} = 1;
	}

	# Show event:
	my $classes;
	if ($list_alternate) { $classes='b'; $list_alternate=0; } else { $classes='a'; $list_alternate=1; }
	if ($mobile==0) {
		if ($doMouseover) {
			# If we're in popup-search, highlight in different color:
			my $listactiveOverwrite='';
			if ($popupMode) { $listactiveOverwrite=',\'#69d74c\''; }
			if ($Location && !$popupMode) {
				$str.= "<li class='$classes' id='event_loc_$Location->{id}' onmouseover=\"denkmal_mousein('$Location->{id}','$location_name_',1,$dotsize,1$listactiveOverwrite);\" onmouseout=\"denkmal_mouseout('$Location->{id}',$dotsize,1);\">";
			} else {
				$NoLocCounter++;
				$str.= "<li class='$classes' id='event_loc_tmp_$NoLocCounter' onmouseover=\"denkmal_mousein('$NoLocCounter','$location_name_',1,$dotsize,0$listactiveOverwrite);\" onmouseout=\"denkmal_mouseout('$NoLocCounter',$dotsize,0);\">";
			}
		} else {
			$str.= "<li class='$classes'>";
		}
			$str .= "<div class='eventdesc'>";
				if ($Event->{audio}) {
					my $audio_text = $Event->{audio};
					if ($audio_text =~ /^(.+)\.[^\.]{0,6}$/) { $audio_text=$1; }
					$str .= "<div class='eventicon' onmouseover='denkmal_mousein_audio(\"$audio_text\");' onmouseout='denkmal_mouseout_audio();'>";
					if ($popupMode) {
						# We're in popup-mode. Print wimpy player directly:
						$str .= wimpyCodeHtml("$link_prefix$audiodir/" .$Event->{audio}, "eventpl_".$Event->{id}, $wimpyColor);
					} else {
						$str .= wimpyCode("$link_prefix$audiodir/" .$Event->{audio}, "eventpl_".$Event->{id}, $wimpyColor);
					}
				} else {
					$str .= "<div class='eventicon noaudio'>";
					$str .= "<img src='$link_prefix$picsdir/$dot'/>";
				}
				$str .= "</div>";
			if ($showDatum) {
				$str .= "<span class='listdatum'>" .datum_sql2norm($Event->{datum}). "</span> ";
			}
			$str .= "$location_name_linked: $event_zeit$event_beschreibung";
			$str .= "</div>";
		$str .= "</li>\n";
	} else {
		$str .= "<li style='list-style-image: url($picsdir/$dot)' class='$classes'>";
		if ($showDatum) {
			$str .= "<span class='listdatum'>" .datum_sql2norm($Event->{datum}). "</span> ";
		}
		$str .= "$location_name_linked: <i>$event_zeit$event_beschreibung</i>";
		if ($iphone) {
			if ($Event->{audio}) {
				my $audio_text = $Event->{audio};
				if ($audio_text =~ /^(.+)\.[^\.]{0,6}$/) { $audio_text=$1; }
				$str .= "<a class='mp3' href='" .$audiodir. "/" .$Event->{audio}. "'><img src='/pics/play.png' />$audio_text</a>";
			}
		}
		$str .= "</li>\n";
	}

	return $str;
}

sub getLocationDots() {
	# Alle Locations durchgehen:
	my $Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null) and (enabled = '1')");
	$Select->execute; 
	while (my $Location=$Select->fetchrow_hashref) {
		# Check whether to show this location or not:
		if ($Location->{showalways} || $showall == 1) {
			# Check, whether this dot was already printed (with an event):
			if (!$PrintedLocId{$Location->{id}}) {
				# Dot-size:
				my $dotsize=7;
				# Dot html:
				my $dot = "dot2.gif";
				my $location_name_ = $Location->{name};
				$location_name_=~s/'/\\'/g;
				$dots .= "<div id='dot_$Location->{id}' class='dot' style='left:$Location->{posx}px; top:$Location->{posy}px; z-index:1;'><a ";
				if ($Location->{website}) { $dots .= "target='_blank' href='$Location->{website}'"; } else { $dots .= "href='javascript:;'"; }
				$dots .= " onmouseover=\"denkmal_mousein('$Location->{id}','$location_name_',1,$dotsize,1);\" onmouseout=\"denkmal_mouseout('$Location->{id}',$dotsize,1);\"><img src=\"$picsdir/$dot\" id=\"dotimg_$Location->{id}\"/></a></div>";

			}
		}
	}

}



sub listEvents() {
	my $str;
	$str .= listEventsByStar(1);
	$str .= listEventsByStar(0);
	return $str;
}

sub listEventsByStar($) {
	my ($hasStar) = @_;
	if ($hasStar != 1) { $hasStar=0; }
	my $doMouseover = 1;
	if ($widget) { $doMouseover = 0; }
	my $str;

	# Alle Locations durchgehen:
	my $Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null) and (enabled = '1')");
	$Select->execute; 
	while (my $Location=$Select->fetchrow_hashref) {
	
		# Event dieser Location zu diesem tag suchen:
		my $Select2 = $dbh->prepare("SELECT * FROM events WHERE (ort = '" .escapeSql($Location->{name}). "') and (datum = '$datumgesucht_sql') and (enabled = '1') and (blocked = '0')");
		$Select2->execute; 
		my $Event=$Select2->fetchrow_hashref;
		$Select2->finish();

		# Location hat heute einen Event:
		if ($Event->{enabled}) {
			if ($Event->{star} == $hasStar) {

				# Der Eventliste hinzufügen:
				$str .= getEvent($Event, $Location, 0, $doMouseover, 0);
			}
		}
	}
	 $Select->finish();

	
	# Alle Events mit einer nicht eingetragenen Location in die Eventliste hinzufügen:
	$Select = $dbh->prepare("SELECT * FROM events WHERE (datum = '$datumgesucht_sql') and (enabled = '1') and (blocked = '0') and (star = '$hasStar')");
	$Select->execute;
	while (my $Event=$Select->fetchrow_hashref) {
		# Gibt es eine passende Location in der Datenbank?
		my $Select2 = $dbh->prepare("SELECT count(*) FROM locations WHERE (name = '" .escapeSql($Event->{ort}). "') and (enabled = '1')"); $Select2->execute; 
		# Es gibt keine passende Location:
		if ($Select2->fetchrow == 0) {
			# Der Eventliste hinzufügen:
			$str .= getEvent($Event, 0, 0, $doMouseover, 0);
		}
		$Select2->finish;
	}
	$Select->finish();

	return $str;
}



sub listEventsByQuery($$$) {
	my ($q, $qmode, $qtime) = @_;
	my $q = escapeSql($q);
	my $qmode = escapeSql($qmode);
	my $qtime = escapeSql($qtime);
	my $str;
	my $maxresults = 15;
	my $numresults = 0;

	# SQL-Query erstellen:
	my $query = "SELECT * FROM events WHERE (enabled = 1) AND (blocked = 0)";
	if ($qmode eq 'location') { $query .= " AND (ort = '$q')"; }
	else { $query .= " AND ((ort LIKE '%$q%') OR (beschreibung LIKE '%$q%'))"; }

	# Zukünftige Events zu dieser query suchen:
	if ($qtime ne 'past') {
		my $Select = $dbh->prepare("$query AND (datum >= CURDATE()) ORDER BY datum ASC LIMIT ".($maxresults-$numresults)); $Select->execute;
		my $i=0;
		if ($Select->rows != 0) {
			$str .= "<h3>Kommende Events:</h3>";
			$str .= "<ul>";
			while (my $Event=$Select->fetchrow_hashref) {
				$str .= getEventByEvent($Event, 1, 1);
				$i++;
			}
			$str .= "</ul>";
		}
		$numresults+=$i;
	}

	# Reset list-alternate:
	$list_alternate = 0;

	# Vergangene Events zu dieser query suchen:
	if ($qtime ne 'future') {
		my $Select = $dbh->prepare("$query AND (datum < CURDATE()) ORDER BY datum DESC LIMIT ".($maxresults-$numresults)); $Select->execute;
		my $i=0;
		if ($Select->rows != 0) {
			$str .= "<h3>Vergangene Events:</h3>";
			$str .= "<ul>";
			while (my $Event=$Select->fetchrow_hashref) {
				$str .= getEventByEvent($Event, 1, 1);
				$i++;
			}
			$str .= "</ul>";
		}
		$numresults+=$i;
	}

	# Keine Events gefunden:
	if ($numresults==0) {
		my $timeStr='';
		if ($qtime eq 'future') { $timeStr=' zukünftigen'; } elsif ($qtime eq 'past') { $timeStr=' vergangenen'; }
		if ($qmode eq 'location') {
			$str = "<h3>Keine$timeStr Events zu dieser Location gefunden.</h3>";
		} else {
			$str = "<h3>Keine passenden$timeStr Events gefunden.</h3>";
		}
	}


	return $str;
}






sub isIphone() {
	my ($ua) = @_;
	if ($ua =~ /iPhone/i) { return 1; }
	if ($ua =~ /iPod/i) { return 1; }
	
	return 0;
}




sub isMobile() {
	my ($ua) = @_;
	if ($ua =~ /iPhone/i) { return 1; }
	if ($ua =~ /iPod/i) { return 1; }
	if ($ua =~ /Webkit/i && $ua =~ /Mobile/i) { return 1; }
	if ($ua =~ /AvantGo/i) { return 1; }
	if ($ua =~ /DoCoMo/i) { return 1; }
	if ($ua =~ /KDDI/i) { return 1; }
	if ($ua =~ /UP\.Browser/i) { return 1; }
	if ($ua =~ /Vodafone/i) { return 1; }
	if ($ua =~ /J-PHONE/i) { return 1; }
	if ($ua =~ /DDIPOCKET/i) { return 1; }
	if ($ua =~ /PDXGW/i) { return 1; }
	if ($ua =~ /ASTEL/i) { return 1; }
	if ($ua =~ /PalmOS/i) { return 1; }
	if ($ua =~ /Windows CE/i) { return 1; }
	if ($ua =~ /Plucker/i) { return 1; }
	if ($ua =~ /NetFront/i) { return 1; }
	if ($ua =~ /Xiino/i) { return 1; }
	if ($ua =~ /BlackBerry/i) { return 1; }
	if ($ua =~ /iPAQ/i) { return 1; }
	if ($ua =~ /MIDP/i) { return 1; }
	if ($ua =~ /Smartphone/i) { return 1; }
	if ($ua =~ /portalmmm/i) { return 1; }
	if ($ua =~ /SymbianOS/i) { return 1; }
	if ($ua =~ /Series80/i) { return 1; }
	if ($ua =~ /PalmSource/i) { return 1; }
	if ($ua =~ /Blazer/i) { return 1; }
	if ($ua =~ /Opera Mini/i) { return 1; }
	if ($ua =~ /ReqwirelessWeb/i) { return 1; }
	if ($ua =~ /SonyEricsson/i) { return 1; }
	if ($ua =~ /PlayStation Portable/i) { return 1; }
	if ($ua =~ /Sprint/i) { return 1; }
	if ($ua =~ /EudoraWeb/i) { return 1; }
	if ($ua =~ /Elaine/i) { return 1; }

	return 0;
}






# Datum und taggesucht berechnen:
 sub getDatum() {

 ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900;
 my ($today, $todaystamp, $versatz);
 $today = ymd2julian($Jahr,$Monat,$Monatstag);
 $todaystamp = "$Monatstag.$Monat.$Jahr";

 if ($Stunden<$morninghour) {
  ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time -86400); $Monat+=1; $Jahr+=1900;
  $today = ymd2julian($Jahr,$Monat,$Monatstag);
  $todaystamp = "$Monatstag.$Monat.$Jahr";
  $versatz=-86400;
 }

 # Looking for $taggesucht and insering date:
 my $n=0;
 foreach(@tage) { 
  if ($taggesucht =~ /^$_/i) {
   my $weekoffset;
   if ($taggesucht =~ /^$_\_(\d+)$/i) { $weekoffset=$1; }
   if ($n >= $Wochentag) {
    my ($tmp, $tmp, $tmp, $Monatstag2, $Monat2, $Jahr2) = localtime(time + ($n-$Wochentag +7*$weekoffset)*86400 +$versatz); $Monat2+=1; $Jahr2+=1900;
    $datumgesucht = "$Monatstag2.$Monat2.$Jahr2";
	$datumgesucht_sql = datum_sql($Jahr2, $Monat2, $Monatstag2);
    last;
   } else {
    my ($tmp, $tmp, $tmp, $Monatstag2, $Monat2, $Jahr2) = localtime(time + (7-$Wochentag+$n+7*$weekoffset)*86400 +$versatz); $Monat2+=1; $Jahr2+=1900;
    $datumgesucht = "$Monatstag2.$Monat2.$Jahr2";
	$datumgesucht_sql = datum_sql($Jahr2, $Monat2, $Monatstag2);
    last;
   }
  }
  $n++;
 }

 if ($taggesucht =~ /\_(\d+)$/i) { $taggesucht="$tage[$n]_$1"; }
 else { $taggesucht=$tage[$n]; }
 
 if (!$datumgesucht) {
  $datumgesucht = "$Monatstag.$Monat.$Jahr";
  $datumgesucht_sql = datum_sql($Jahr, $Monat, $Monatstag);
  $taggesucht=$tage[$Wochentag];
 } else {

  $pagetitle = $tage[$n];
 }
 
 
 
}

# Wimpy-Flash Player HTML-Javascript-Erzeuger-Code zu einer MP3-URL zurückgeben:
sub wimpyCode($$$) {
	my($audiofile, $id, $bgcolor) = @_;
	my $str='';
	my $size=15;

	$str.= "<div id='wimpy_" .$id. "'>";
	$str.= "<script type='text/javascript'>swfobject.embedSWF(	'$link_prefix/wimpy_button.swf', 'wimpy_" .$id. "', '" .$size. "', '" .$size. "', '9.0.0', false, 
				{theFile:'" .$audiofile. "', playingColor:'" .$bgcolor. "', theBkgdColor:'" .$bgcolor. "', wimpyReg:'NyU3RVVCJTdERyUzQiUyN3AlODAlMkZ5bExzSyU1QyU1Qk4lM0VldCUzRDVyJTVFWk8lN0N0'},
				{wmode:'transparent'});</script>";
	$str.= "</div>";
	return $str;
}

# Wimpy-Flash Player direkter HTML-Code zu einer MP3-URL zurückgeben:
sub wimpyCodeHtml($$$) {
	my($audiofile, $id, $bgcolor) = @_;
	my $str='';
	my $size=15;

	$str.= "<embed src='/wimpy_button.swf' flashvars='theFile=$audiofile&playingColor=$bgcolor&theBkgdColor=$bgcolor&wimpyReg=NyU3RVVCJTdERyUzQiUyN3AlODAlMkZ5bExzSyU1QyU1Qk4lM0VldCUzRDVyJTVFWk8lN0N0' width='$size' height='$size' quality='high' type='application/x-shockwave-flash' wmode='transparent'  name='wimpy_$id' />";
	return $str;
}
 




# Get URLs from DB and cache them into a hash so we can insert them into events:
sub getUrls() {
	my $Select = $dbh->prepare("SELECT name, url, onlyifmarked, alias, CHAR_LENGTH(name) AS len
								FROM urls 
								WHERE (alias IS NULL)
								ORDER BY len DESC");
	$Select->execute;
	# Loop:
	while (my $Row=$Select->fetchrow_hashref) {
		push(@url_Name, $Row->{name});
		push(@url_Url, $Row->{url});
		$NameToOnlyifmarked{$Row->{name}} = $Row->{onlyifmarked};
	}
}




## In einem String URLs durch HTML-Links ersetzen:
sub add_links{
 my($src) = @_;

 # Replace urls:
 # Ohne Pfad:
 $src =~ s/((http:\/\/)?((www)?[\w_\.\-öäü]{5,}\.[a-z]{2,4}))\b/<a class='url' href='http:\/\/$3' target='_blank'>$1<\/a>/gi;
 # Mit Pfad nach domain:
 $src =~ s/((http:\/\/)?((www)?[\w_\.\-öäü]{5,}\.[a-z]{2,4}\/[\/\w_\.\-öäü]*))\b/<a class='url' href='http:\/\/$3' target='_blank'>$1<\/a>/gi;

 # Replace urls in hash (from db):
 my $i=0;
 foreach my $urlName (@url_Name) {
	my $urlUrl = $url_Url[$i];
	
	if ($NameToOnlyifmarked{$urlName}) {
		$src =~ s/\[$urlName\]/<a class='url' href='$urlUrl' target='_blank'>$urlName<\/a>/i;
	} else {
		if (! ($src =~ s/\b$urlName\b/<a class='url' href='$urlUrl' target='_blank'>$urlName<\/a>/i)) {
			if (! ($src =~ s/\b$urlName([:\s])/<a class='url' href='$urlUrl' target='_blank'>$urlName<\/a>$1/i)) {
				$src =~ s/(\s)$urlName([:\s])/$1<a class='url' href='$urlUrl' target='_blank'>$urlName<\/a>$2/i;
			}	
		}
	}
	$i++;
  }

 return $src;
}



## SQL-Datum erhalten:
sub datum_sql{
 my($y, $m, $d) = @_;
 if ($d<10) { $d = "0$d"; }
 if ($m<10) { $m = "0$m"; }
 if ($y<100) { $y = "19$y"; }
 return "$y-$m-$d";
}

## 1 bis 2 SQL Zeiten formatieren:
sub zeitformat{
 my($zeit,$zeit2) = @_;
 if ($zeit2) { return zeit_sql2norm($zeit) ."-". zeit_sql2norm($zeit2); }
 else { return zeit_sql2norm($zeit); }
}

## SQL-Zeit in Form "HH:MM" umwandeln:
sub zeit_sql2norm{
 my($zeit) = @_;
 if ($zeit =~ /^0(\d:\d\d):\d\d$/) { return $1; }
 elsif ($zeit =~ /^(\d\d:\d\d):\d\d$/) { return $1; }
 else { return $zeit; }
}

## SQL-Datum in Form "TT.MM.JJJJ" umwandeln:
sub datum_sql2norm{
 my($datum) = @_;
 if ($datum =~ /^0*(\d+)-0*(\d+)-0*(\d+)$/) { 
	my $y=$1; my $m=$2; my $d=$3;
	if ($d=~/^\d$/) { $d="&nbsp;$d"; }
	$m = $monate_kurz[$m-1];
	if ($y=~/^\d{2}(\d{2})$/) { $y=$1; }
	return "$d.$m'$y";
 }
 else { return $datum; }
}

## SQL-Datum in Form y,m,d umwandeln:
sub datum_sql2ymd{
 my($datum) = @_;
 if ($datum =~ /^(\d+)-(\d+)-(\d+)$/) { 
	return ($1, $2, $3);
 }
 return;
}


 ## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
	my($y, $m, $d) = @_;
	my $f;
	if ($m < 3) { $f = -1; } else { $f = 0; }
	return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
} 
 
 
 
 
# Escapes a string to be savely used in a SQL-query
sub escapeSql($) {
	my ($str) = @_;
	$str =~ s/\\/\\\\/g;
	$str =~ s/'/\\'/g;
	return $str;
}