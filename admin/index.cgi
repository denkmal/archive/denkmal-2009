#!/usr/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use DBI;
use CGI;
use POSIX;
use Digest::MD5 qw(md5_base64);

# ÜBERGABEN
$query = CGI::new();
my $action = $query->param("action");
my $page = $query->param("page");
my $location = $query->param("loc");
my $taggesucht = $query->param("tag");

# Wann beginnt der neue Tag:
my $morninghour = 6;
# DATUMZEUGS und gesuchter Tag suchen:
my ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag);
our $datumgesucht, $datumgesucht_sql;
my @tage = ("So","Mo","Di","Mi","Do","Fr","Sa");
&getDatum();

# Relativer Pfad vom Client zu den Audio Dateien:
my $audiodir = "/audio";
# Relativer Pfad zu den Audio Dateien auf dem Server:
my $audiodirloc = "../audio";



# DATENBANK
$dbh = DBI->connect("DBI:mysql:denkmal_org:localhost","denkmal_org","thedog")  || die "Database error: $DBI::errstr";
$set = $Select2 = $dbh->prepare("SET NAMES utf8");
$set->execute; $set->finish;

# LOGIN
$auth_must_type = 10;
$header = "Content-Type: text/html\n\n";
$header.= "<html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'></head><body>";
do "auth.cgi";








 if ($action) {
	my $db = $query->param("db");
	my $id = $query->param("id");
	
	if ($db eq "locations" || $db eq "events" || $db eq "urls" || $db eq 'wettbewerb' || $db eq 'wettbewerb_teilnehmer') {
		if ($action eq "del") {
			$edit = $dbh->prepare("DELETE FROM $db WHERE (id = '$id')");
			
		} elsif ($action eq "disable") {
			$edit = $dbh->prepare("UPDATE $db SET enabled='0' WHERE (id = '$id')");
		} elsif ($action eq "enable") {
			$edit = $dbh->prepare("UPDATE $db SET enabled='1' WHERE (id = '$id')");
			if ($db eq "events") {
				# Schon existierende Einträge an diesem Tag in dieser Location in der DB löschen:
				$Select = $dbh->prepare("SELECT * FROM $db WHERE (id = '$id')");
				$Select->execute; $Row=$Select->fetchrow_hashref;
				$del = $dbh->prepare("DELETE FROM events WHERE ((datum = '$Row->{datum}') and (ort = '$Row->{ort}') and (nodeletion = '0') and (id <> '$id'))");
				$del->execute;
				$del->finish;
				$Select->finish;
			}
		} elsif ($action eq "star") {
			$edit = $dbh->prepare("UPDATE $db SET star='1' WHERE (id = '$id')");
		} elsif ($action eq "unstar") {
			$edit = $dbh->prepare("UPDATE $db SET star='0' WHERE (id = '$id')");
			
		} elsif ($action eq "starprotect") {
			$edit = $dbh->prepare("UPDATE $db SET star='1', nodeletion='1' WHERE (id = '$id')");
		} elsif ($action eq "unstarunprotect") {
			$edit = $dbh->prepare("UPDATE $db SET star='0', nodeletion='0' WHERE (id = '$id')");
			
		} elsif ($action eq "protect") {
			$edit = $dbh->prepare("UPDATE $db SET nodeletion='1' WHERE (id = '$id')");
		} elsif ($action eq "unprotect") {
			$edit = $dbh->prepare("UPDATE $db SET nodeletion='0' WHERE (id = '$id')");
			
		} elsif ($action eq "block") {
			$edit = $dbh->prepare("UPDATE $db SET blocked='1', nodeletion='1' WHERE (id = '$id')");
		} elsif ($action eq "unblock") {
			$edit = $dbh->prepare("UPDATE $db SET blocked='0', nodeletion='0' WHERE (id = '$id')");
			
		} elsif ($action eq "chgbeschreibung") {
			$q_beschreibung=$query->param('beschreibung');
			$q_beschreibung=~s/\'/\\'/g;
			$edit = $dbh->prepare("UPDATE $db SET beschreibung='" .$q_beschreibung. "', nodeletion='1' WHERE (id = '$id')");


		} elsif ($action eq "setaudio") {
			# Set the wished audio file:
			my $audiofile = $query->param("audiofile");
			$edit = $dbh->prepare("UPDATE $db SET audio='$audiofile', nodeletion='1' WHERE (id = '$id')");
			$edit->execute;
			$edit->finish;
			print "<script>window.opener.location.href=\"?tag=$taggesucht&page=$page&loc=$location\"; window.close();</script>";
			# Drop Cache:
			dropCache();
			exit;

		} elsif ($action eq "delaudio") {
			$edit = $dbh->prepare("UPDATE $db SET audio=NULL WHERE (id = '$id')");
		
		} elsif ($action eq "addurl") {
			my $name=$query->param("name"); $name=~s/\'/\\'/g;
			my $url=$query->param("url"); $url=~s/\'/\\'/g;
			if ($name && $url) {
				my $onlyifmarked=0;	if ($query->param("onlyifmarked")) { $onlyifmarked=1; }
				$edit = $dbh->prepare("INSERT INTO urls (name,url,onlyifmarked,enabled) VALUES('$name','$url','$onlyifmarked','1')");
			}
			$page = 'urllist';

		} elsif ($action eq "delurl") {
			$edit = $dbh->prepare("DELETE FROM urls WHERE (id = '$id')");
			$page = 'urllist';

		} elsif ($action eq "disablewettbewerb") {
			$edit = $dbh->prepare("UPDATE $db SET enabled='0' WHERE (id = '$id')");
			$page = 'wettbewerblist';
		} elsif ($action eq "enablewettbewerb") {
			$edit = $dbh->prepare("UPDATE $db SET enabled='1' WHERE (id = '$id')");
			$page = 'wettbewerblist';

		} elsif ($action eq 'delwettbewerbuser') {
			$edit = $dbh->prepare("DELETE FROM wettbewerb_teilnehmer WHERE (id = '$id')");
			$page = 'wettbewerbusers';


		}

		if ($edit) {
			$edit->execute;
			$edit->finish;
			# Drop Cache:
			dropCache();
		}
	}


	# Reset stats:
	if ($action eq "reset_hits_mobile") {
		$edit = $dbh->prepare("UPDATE stats SET value=0 WHERE name='hits_mobile'");
		$edit->execute;
		$edit->finish;
	}
	if ($action eq "reset_hits_iphone") {
		$edit = $dbh->prepare("UPDATE stats SET value=0 WHERE name='hits_iphone'");
		$edit->execute;
		$edit->finish;
	}


	
	if ($action eq "listaudio") {
		# Show a list of available audio files to chose from:
		print "<b>Chose your audio file for event $id:</b><br>";
		opendir(DIR,$audiodirloc) || die "Can't open directory $audiodirloc";
		@audiofiles = sort(readdir(DIR));
		closedir(DIR);
		my $i=0;
		foreach $audiofile (@audiofiles) {
			if ($audiofile !~ /^\./) {
				my $loc2 = $location; $loc2 =~ s/'/&apos;/g;	#'
				print "<a href='?page=$page&loc=$loc2&action=setaudio&id=$id&audiofile=$audiofile&tag=$taggesucht&db=events'>$audiofile</a><br>";
				$i++;
			}
		}
		exit;

	}
 
 
 }




my $loc = $location; $loc =~ s/'/\\'/g;
my $loc2 = $location; $loc2 =~ s/'/&apos;/g;	#'

print qq[
<style>
.editable {
	display: inline;
	font-family: Times;
	font-style: italic;
	font-size: 1em;
}
input.editable, textarea.editable {
	border: 0;
	background-color: red;
	color: white;
	padding: 0;
	margin: 0;
}
form.editable {
	display: inline;
}
</style>
<script>
 function makeEditable(textid, db, id, action, fieldname) {
	var editid=textid+"_edit";
	var textobj=document.getElementById(textid);
	var editobj=document.getElementById(editid);
	if (textobj.style.display!='none') {
		var breite = textobj.offsetWidth +7;
		var hoehe = textobj.offsetHeight;
		textobj.style.display='none';
		if (hoehe>20) {	// Mehrzeilig!
			if (breite>600) { breite=600; }
		}
		var edit='';
		edit +=		'<form class="editable">';
		edit +=		'<input name="' +fieldname+ '" class="editable" id="' +editid+ '_inp" type="text" value="' +textobj.innerHTML+ '"';
		edit +=			' onBlur="makeUneditable(\\'' +editid+ '\\',\\'' +textid+ '\\');"';
		edit +=			' style="width:' +breite+ 'px;">';
		edit +=		'<input type="hidden" name="action" value="' +action+ '">';
		edit +=		'<input type="hidden" name="page" value="$page">';
		edit +=		"<input type='hidden' name='loc' value='$loc2'>";
		edit +=		'<input type="hidden" name="id" value="' +id+ '">';
		edit +=		'<input type="hidden" name="db" value="' +db+ '">';
		edit +=		'<input type="hidden" name="tag" value="$taggesucht">';
		edit +=		'</form>';
		editobj.innerHTML=edit;
		document.getElementById(editid+'_inp').focus();

	}
 
 }
 
 function makeUneditable(editid, textid) {
	var editobj=document.getElementById(editid)
	var textobj=document.getElementById(textid)
	textobj.style.display='inline';
	editobj.innerHTML='';
 }


 function setAudio(id, tag) {
  window.open("?page=$page&loc=$loc&action=listaudio&id="+id+'&tag='+tag, 'Select Audio File', 'toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1,width=350,height=600');
 }
</script>
];





# Locationslist:
 if ($page eq "locationslist") {

	print "<h3 style='margin-bottom: 0px;'>Locations</h3>";
	print "<a href='/admin/'>&lt; home</a><br><br>";

	# Loop through locs:
	 $Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null OR alias='block') ORDER BY name");
	 $Select->execute; 
	 while ($Row=$Select->fetchrow_hashref) {


		# Number of upcoming events:
		my $loc = $Row->{name}; $loc =~ s/'/\\'/g;
		$Select2 = $dbh->prepare("SELECT count(*) FROM events WHERE (ort = '$loc' AND datum >= '$Jahr-$Monat-$Monatstag')"); $Select2->execute; 
		$num_events = $Select2->fetchrow; $Select2->finish();

		if ($Row->{alias} eq 'block') {
			print "[blocked] $Row->{name}";
		} else {
			print "<a href=\"?page=eventslistbyloc&loc=$Row->{name}\">[$num_events events]</a> ";
			if ($Row->{website}) { print "<a href='$Row->{website}'>$Row->{name}</a>"; }
			else { print "<span style='color:blue;'>$Row->{name}</span>"; }
		}

		# Find aliases:
		$Select2 = $dbh->prepare("SELECT * FROM locations WHERE (alias = '" .escapeSql($Row->{name}). "') ORDER BY name");
		$Select2->execute;
		my @aliases;
		while ($Alias=$Select2->fetchrow_hashref) {
			push(@aliases, $Alias->{name});
		}
		if (@aliases) { print ' ("' . join('", "', @aliases) . '")'; }
		$Select2->finish();
		print "<br>";
	 }
	 $Select->finish();

	print "</body></html>";
	exit;
 }



# Eventslist of one location:
 if ($page eq "eventslistbyloc") {

	print "<h3 style='margin-bottom: 0px;'>Upcoming events for '$location'</h3>";
	print "<a href='/admin/'>&lt; home</a> <a href='?page=locationslist'>&lt; listlocs</a><br><br>";

	# Loop through wettbewerbe:
	 my $loc = $location; $loc =~ s/'/\\'/g;
	 $Select = $dbh->prepare("SELECT * FROM events WHERE (ort = '$loc' AND datum >= '$Jahr-$Monat-$Monatstag') ORDER BY datum");
	 $Select->execute; 
	 while ($Row=$Select->fetchrow_hashref) {
	 	listEvent($Row->{id}, $Row->{datum}, $Row->{zeit}, $Row->{zeit2}, $Row->{ort}, $Row->{beschreibung}, $Row->{star}, $Row->{nodeletion}, $Row->{blocked}, $Row->{enabled}, $Row->{audio}, 1);
	 }
	 $Select->finish();

	print "</body></html>";
	exit;
 }





# Wettbewerb-List:
 if ($page eq "wettbewerblist") {
	print "<h3 style='margin-bottom: 0px;'>Wettbewerbe</h3>";
	print "<a href='/admin/'>&lt; home</a><br>";

	print "<br>";

	# Loop through wettbewerbe:
	 $Select = $dbh->prepare("SELECT * FROM wettbewerb ORDER BY enabled DESC, id DESC");
	 $Select->execute; 
	 while ($Row=$Select->fetchrow_hashref) {

		# Print one row:
		if ($Row->{enabled}==1) {
			print "<a href='?action=disablewettbewerb&db=wettbewerb&id=$Row->{id}'>[x]</a>";
		} else {
			print "<a href='?action=enablewettbewerb&db=wettbewerb&id=$Row->{id}'>[&nbsp;]</a>";
		}
		print " <a href='?page=wettbewerbranduser&wettbewerbid=$Row->{id}'>[rand]</a>";
		print " <a href='?page=wettbewerbusers&wettbewerbid=$Row->{id}'>$Row->{name}</a>";

		# Find users for this wettbewerb:
		$Select2 = $dbh->prepare("SELECT COUNT(1) as num FROM wettbewerb_teilnehmer WHERE (wettbewerbid=$Row->{id})");
		$Select2->execute;
		$Teilnehmer=$Select2->fetchrow_hashref;
		print " ($Teilnehmer->{num})";
		$Select2->finish();
		print "<br>\n";
	 }
	 $Select->finish();

	print "</body></html>";
	exit;
 }

# Random Wettbewerb-User:
 if ($page eq "wettbewerbranduser") {
	my $wettbewerbid = $query->param("wettbewerbid");
	my $Select = $dbh->prepare("SELECT * FROM wettbewerb WHERE (id=$wettbewerbid)");
	$Select->execute;
	my $Wett=$Select->fetchrow_hashref;
	$Select->finish;

	print "<h3 style='margin-bottom: 0px;'>Random Wettbewerb-User for '$Wett->{name}'</h3>";
	print "<a href='/admin/'>&lt; home</a> <a href='?page=wettbewerblist'>&lt; wettbewerbe</a> <a href='?page=wettbewerblist'>&lt; wettbewerb</a><br>";

	print "<br>";

	# Get random user:
	 my $Select2 = $dbh->prepare("SELECT * FROM wettbewerb_teilnehmer WHERE (wettbewerbid=$wettbewerbid) ORDER BY rand() limit 1");
	 $Select2->execute; 
	 my $RandUser=$Select2->fetchrow_hashref;
	 $Select2->finish;

	# Print one row:
	print "$RandUser->{name} ($RandUser->{email})";
	print "<br>\n";
	 $Select->finish();

	print "</body></html>";
	exit;
 }

# Wettbewerb-User-List:
 if ($page eq "wettbewerbusers") {
	my $wettbewerbid = $query->param("wettbewerbid");
	my $Select = $dbh->prepare("SELECT * FROM wettbewerb WHERE (id=$wettbewerbid)");
	$Select->execute;
	my $Wett=$Select->fetchrow_hashref;
	$Select->finish;

	print "<h3 style='margin-bottom: 0px;'>Wettbewerb-Users for '$Wett->{name}'</h3>";
	print "<a href='/admin/'>&lt; home</a> <a href='?page=wettbewerblist'>&lt; wettbewerbe</a><br>";

	print "<br>";

	# Loop through locs:
	 my $Select2 = $dbh->prepare("SELECT * FROM wettbewerb_teilnehmer WHERE (wettbewerbid=$wettbewerbid) ORDER BY datum DESC, zeit DESC");
	 $Select2->execute; 
	 while ($Row=$Select2->fetchrow_hashref) {

		# Print one row:
		print "<a href='?action=delwettbewerbuser&id=$Row->{id}&wettbewerbid=$Wett->{id}&db=wettbewerb_teilnehmer'>[del]</a>";
		print " $Row->{name} ($Row->{email})";


		print "<br>\n";
	 }
	 $Select->finish();

	print "</body></html>";
	exit;
 }




# URL-List:
 if ($page eq "urllist") {

	print "<h3 style='margin-bottom: 0px;'>URLs</h3>";
	print "<a href='/admin/'>&lt; home</a><br>";

	print "<form method='post'><input type='hidden' name='action' value='addurl'><input type='hidden' name='db' value='urls'>";
	print "name: <input type='text' name='name'> url: <input type='text' name='url' value='http://'> onlyifmarked: <input type='checkbox' name='onlyifmarked'> <input type='submit' value='New link'><br>";
	print "</form>";

	print "<br>";

	# Loop through locs:
	 $Select = $dbh->prepare("SELECT * FROM urls WHERE (alias is null) ORDER BY name");
	 $Select->execute; 
	 while ($Row=$Select->fetchrow_hashref) {

		# Print one row:
		print "<a href='?action=delurl&db=urls&id=$Row->{id}'>[del]</a> ";
		print "$Row->{name}: ";
		my $url=$Row->{url}; $url=~ s/^http:\/\///i;
		print "<a href='$Row->{url}'>$url</a>";
		if ($Row->{onlyifmarked}) { print " <span style='color:#aaaaaa;'>(onlyifmarked)</span>"; }

		# Find aliases for this row:
		$Select2 = $dbh->prepare("SELECT * FROM urls WHERE (alias = '" .escapeSql($Row->{name}). "') ORDER BY name");
		$Select2->execute;
		my @aliases;
		while ($Alias=$Select2->fetchrow_hashref) {
			push(@aliases, $Alias->{name});
		}
		if (@aliases) { print ' ("' . join('", "', @aliases) . '")'; }
		$Select2->finish();
		print "<br>\n";
	 }
	 $Select->finish();

	print "</body></html>";
	exit;
 }






# Admin-Links:
 print '<hr>';
 print '<a href="?page=locationslist">listlocs</a>';
 print ' * ';
 print '<a href="?page=urllist">listurls</a>';
 print ' * ';
 print '<a href="?page=wettbewerblist">wettbewerbe</a>';
 print ' * ';
 print '<a href="http://www.denkmal.org/?showall=1">denkmal.org</a> <a href="/mappoint.html">mappoint</a> <a href="http://www.mybasel.ch/">mybasel</a> <a href="http://62.75.148.231/phpmyadmin/">phpmyadmin</a><br>';

# Mobile-Stats:
 $Select = $dbh->prepare("SELECT * FROM stats WHERE name='hits_mobile'");
 $Select->execute; $Row=$Select->fetchrow_hashref;
 print "Mobile hits: $Row->{value} (<a href='?action=reset_hits_mobile'>reset</a>)";
 print ' * ';
 $Select = $dbh->prepare("SELECT * FROM stats WHERE name='hits_iphone'");
 $Select->execute; $Row=$Select->fetchrow_hashref;
 print "iPhone hits: $Row->{value} (<a href='?action=reset_hits_iphone'>reset</a>)<br>";
 print '<br>';


 # Denkmal / SM - Sound of the Moment:
 $Select = $dbh->prepare("SELECT * FROM events WHERE (ort='denkmal_sm' AND id=-100 AND enabled=1)");
 $Select->execute; 
 if ($Select->rows > 0) {
	print '<hr>';
	print '<h3 style="margin-bottom: 0px;">Sound of the Moment</h3>';
	$Row=$Select->fetchrow_hashref;
	listEvent($Row->{id}, $Row->{datum}, $Row->{zeit}, $Row->{zeit2}, $Row->{ort}, $Row->{beschreibung}, $Row->{star}, $Row->{nodeletion}, $Row->{blocked}, $Row->{enabled}, $Row->{audio});
 }


 # Disabled Locations:
 $Select = $dbh->prepare("SELECT * FROM locations WHERE (enabled = '0') ORDER BY name");
 $Select->execute; 
 if ($Select->rows > 0) {
	 print '<hr>';
	 print '<h3 style="margin-bottom: 0px;">Disabled Locations</h3>';
	 while ($Row=$Select->fetchrow_hashref) {
		print "<a href='?action=del&db=locations&id=$Row->{id}&tag=$taggesucht'>[del]</a> <a href='?action=enable&db=locations&id=$Row->{id}&tag=$taggesucht'>[enable]</a> ";
		print "$Row->{name} - <a href='$Row->{website}'>$Row->{website}</a><br>";
	 }
 }

 $Select->finish();


 # Disabled Events
 $Select = $dbh->prepare("SELECT * FROM events WHERE (enabled = '0') ORDER BY datum");
 $Select->execute;
 if ($Select->rows > 0) {
	 print '<hr>';
	 print '<h3>Disabled Events</h3>';
	 while ($Row=$Select->fetchrow_hashref) {
		listEvent($Row->{id}, $Row->{datum}, $Row->{zeit}, $Row->{zeit2}, $Row->{ort}, $Row->{beschreibung}, $Row->{star}, $Row->{nodeletion}, $Row->{blocked}, $Row->{enabled}, $Row->{audio});
	 }
 }
 $Select->finish();
 
 
 # Links zu anderen Wochentagen:
 print '<hr>';
 $n=$Wochentag;
 foreach(@tage) {
  if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { print "<b>"; }
  print "<a href=\"?tag=$tage[$n]\">$tage[$n]</a> ";
  if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { print "</b>"; }
  $n++; if($n>6) {$n=0;} 
 }
 # Plus zwei Tage mehr:
 $n=$Wochentag;
 for ($i=0; $i<2; $i++) {
  if ($taggesucht eq "$tage[$n]_1" || (!$taggesucht && $Wochentag==$n)) { print "<b>"; }
  print "<a href=\"?tag=$tage[$n]_1\">$tage[$n]+1</a> ";
  if ($taggesucht eq "$tage[$n]_1" || (!$taggesucht && $Wochentag==$n)) { print "</b>"; }
  $n++; if($n>6) {$n=0;} 
 }


 # Blocked Events

 $Select = $dbh->prepare("SELECT * FROM events WHERE (blocked = '1') and (enabled = '1') and (datum = '$datumgesucht_sql') ORDER BY ort");
 $Select->execute;
 if ($Select->rows > 0) {
	 print '<hr>';
	 print '<h3>Blocked Events</h3>';
	 while ($Row=$Select->fetchrow_hashref) {
		  listEvent($Row->{id}, $Row->{datum}, $Row->{zeit}, $Row->{zeit2}, $Row->{ort}, $Row->{beschreibung}, $Row->{star}, $Row->{nodeletion}, $Row->{blocked}, $Row->{enabled}, $Row->{audio});
	 }
 }
 $Select->finish();
 

 # Enabled Events
 print '<hr>';
 print '<h3>Enabled Events</h3>';
 $Select = $dbh->prepare("SELECT * FROM events WHERE (blocked = '0') and (enabled = '1') and (datum = '$datumgesucht_sql') ORDER BY ort");
 $Select->execute; 
 while ($Row=$Select->fetchrow_hashref) {
	listEvent($Row->{id}, $Row->{datum}, $Row->{zeit}, $Row->{zeit2}, $Row->{ort}, $Row->{beschreibung}, $Row->{star}, $Row->{nodeletion}, $Row->{blocked}, $Row->{enabled}, $Row->{audio});
 }
 $Select->finish();
 


 
 print '</body></html>';
 
 
 
 $dbh->disconnect();
 
 
 
 
 
 
 
 # Einen Event als HTML ausgeben:
 sub listEvent() {
	my($id, $datum, $zeit1, $zeit2, $wo, $was, $star, $nodeletion, $blocked, $enabled, $audio, $showDatum) = @_;
 
 	# Überprüfen, ob es die beim Event eingetragene Location schon in der DB gibt:
	$wo_=$wo; $wo_=~s/\'/\\'/g;
	$Select2 = $dbh->prepare("SELECT count(*) FROM locations WHERE (name = '$wo_') and (enabled = '1')"); $Select2->execute; 
	$location_ex = $Select2->fetchrow; $Select2->finish();

	my $loc = $location; $loc =~ s/'/&apos;/g;	#'
	
	
	# First Icon: Star?
	if ($star == 1) { 
		my $tmp_action="unstarunprotect"; if ($enabled==0 || $blocked==1) { $tmp_action="unstar"; }
		print "<a href='?page=$page&loc=$loc&action=$tmp_action&db=events&id=$id&tag=$taggesucht'><img src='/pics/dot3.gif' border='0'></a> ";
	} else { 
		my $tmp_action="starprotect"; if ($enabled==0 || $blocked==1) { $tmp_action="star"; }
		print "<a href='?page=$page&loc=$loc&action=starprotect&db=events&id=$id&tag=$taggesucht'><img src='/pics/dot1.gif' border='0' style='margin-right:2px;'></a> ";
	}

	# Second Icon: Protected?
	if ($nodeletion == 1) {
		print "<a href='?page=$page&loc=$loc&action=unprotect&db=events&id=$id&tag=$taggesucht'><img src='/pics/lock.gif' border='0'></a> ";
	} else {
		print "<a href='?page=$page&loc=$loc&action=protect&db=events&id=$id&tag=$taggesucht'><img src='/pics/nolock.gif' border='0'></a> ";
	}
	
	# Button: DEL
	print "<a href='?page=$page&loc=$loc&action=del&db=events&id=$id&tag=$taggesucht'>[del]</a> ";
	
	# Button: ENABLED/DISABLE
	if ($enabled == 1) {
		print "<a href='?page=$page&loc=$loc&action=disable&db=events&id=$id&tag=$taggesucht'>[disable]</a> ";
	} else {
		print "<a href='?page=$page&loc=$loc&action=enable&db=events&id=$id&tag=$taggesucht'>[enable]</a> ";
	}
	
	# Button: BLOCK/UNBLOCK
	if ($enabled == 1) {
		if ($blocked == 1) {
			print "<a href='?page=$page&loc=$loc&action=unblock&db=events&id=$id&tag=$taggesucht'>[unblock]</a> ";
		} else {
			print "<a href='?page=$page&loc=$loc&action=block&db=events&id=$id&tag=$taggesucht'>[block]</a> ";
		}
	}

	# Button: AUDIO-FILE
	if ($audio) {
		print "<a href='?page=$page&loc=$loc&action=delaudio&db=events&id=$id&tag=$taggesucht'>[<b>mp3</b>]</a> ";
	} else {
		print "<a href='javascript:;' onClick='setAudio(\"$id\", \"$taggesucht\");'>[mp3]</a> ";
	}
	
	print "<font style='color:#888888;'>";
	
	# Datum:
	if ($enabled == 0 || $showDatum == 1) {
		print datum_sql2norm($datum) .", ";
	}
	
	# Zeit:
	print zeitformat($zeit1,$zeit2);
	
	print "</font>&nbsp;-&nbsp;";
	
	# Ort:
	if ($location_ex == 0) {
		print "<font style='color:#BB0000;'>$wo</font>";
	} else { 
		$Select3 = $dbh->prepare("SELECT * FROM locations WHERE (name = '$wo_') and (enabled = '1')"); $Select3->execute;
		$loc = $Select3->fetchrow_hashref;
		if ($loc->{website}) { print "<a href='$loc->{website}' style='color:#000000;'>$wo</a>"; }
		else { print "<font style='color:#000000;'>$wo</font>"; }
		$Select3->finish();
	}
	
	# Beschreibung:
	print ": ";
	print "<div id='$id\_was' class='editable' onClick='makeEditable(this.id,\"events\",\"$id\",\"chgbeschreibung\",\"beschreibung\");'>$was</div>";
	print "<div id='$id\_was_edit' class='editable'></div>";


	print "<br>\n";
 
 }
 
 
 
 
 
 
 
 
 
 
 
 sub getDatum() {

 ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900;
 my $today, $todaystamp, $versatz;
 $today = ymd2julian($Jahr,$Monat,$Monatstag);
 $todaystamp = "$Monatstag.$Monat.$Jahr";

 if ($Stunden<$morninghour) {
  ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time -86400); $Monat+=1; $Jahr+=1900;
  $today = ymd2julian($Jahr,$Monat,$Monatstag);
  $todaystamp = "$Monatstag.$Monat.$Jahr";
  $versatz=-86400;
 }

 # Looking for $taggesucht and insering date:
 $n=0;
 foreach(@tage) { 
  if ($taggesucht =~ /^$_/i) {
   my $weekoffset;
   if ($taggesucht =~ /^$_\_(\d+)$/i) { $weekoffset=$1; }
   if ($n >= $Wochentag) {
    ($tmp, $tmp, $tmp, $Monatstag2, $Monat2, $Jahr2) = localtime(time + ($n-$Wochentag +7*$weekoffset)*86400 +$versatz); $Monat2+=1; $Jahr2+=1900;
    $datumgesucht = "$Monatstag2.$Monat2.$Jahr2";
	$datumgesucht_sql = datum_sql($Jahr2, $Monat2, $Monatstag2);
    last;
   } else {
    ($tmp, $tmp, $tmp, $Monatstag2, $Monat2, $Jahr2) = localtime(time + (7-$Wochentag+$n+7*$weekoffset)*86400 +$versatz); $Monat2+=1; $Jahr2+=1900;
    $datumgesucht = "$Monatstag2.$Monat2.$Jahr2";
	$datumgesucht_sql = datum_sql($Jahr2, $Monat2, $Monatstag2);
    last;
   }
  }
  $n++;
 }

 if ($taggesucht =~ /\_(\d+)$/i) { $taggesucht="$tage[$n]_$1"; }
 else { $taggesucht=$tage[$n]; }
 
 if (!$datumgesucht) {
  $datumgesucht = "$Monatstag.$Monat.$Jahr";
  $datumgesucht_sql = datum_sql($Jahr, $Monat, $Monatstag);
  $taggesucht=$tage[$Wochentag]; 
 }
 
 
}

 
 


# Deletes all cache-rows in DB, so everything is new calculated:
sub dropCache() {
	my $del = $dbh->prepare("DELETE FROM cache WHERE (1)");
	$del->execute;
	$del->finish;
}
 
 





 
## SQL-Datum erhalten:
sub datum_sql{
 my($y, $m, $d) = @_;
 if ($d<10) { $d = "0$d"; }
 if ($m<10) { $m = "0$m"; }
 if ($y<100) { $y = "19$y"; }
 return "$y-$m-$d";
}
 
 
 
 ## 1 bis 2 SQL Zeiten formatieren:
sub zeitformat{
 my($zeit,$zeit2) = @_;
 if ($zeit2) { return zeit_sql2norm($zeit) ."-". zeit_sql2norm($zeit2); }
 else { return zeit_sql2norm($zeit); }
}

## SQL-Zeit in Form "HH:MM" umwandeln:
sub zeit_sql2norm{
 my($zeit) = @_;
 if ($zeit =~ /^0(\d:\d\d):\d\d$/) { return $1; }
 elsif ($zeit =~ /^(\d\d:\d\d):\d\d$/) { return $1; }
 else { return $zeit; }
}

## SQL-Datum in Form "DD.MM.YYYY" umwandeln:
sub datum_sql2norm{
 my($datum) = @_;
 if ($datum =~ /^(\d{4})-(\d{1,2})-(\d{1,2})$/) { return "$3.$2.$1"; }
 else { return $datum; }
}


 ## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
 my($y, $m, $d) = @_;
  if ($m < 3) { $f = -1; } else { $f = 0; }
  return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
} 
 


# Escapes a string to be savely used in a SQL-query
sub escapeSql($) {
	my ($str) = @_;
	$str =~ s/\\/\\\\/g;
	$str =~ s/'/\\'/g;
	return $str;
}