#!/usr/bin/perl

use DBI;
use LWP::UserAgent;
use CGI::Carp qw(fatalsToBrowser);
use Encode;

my $debug = 0;

# Number of days to update from today:
$updatedays = 11;

# Aktuelles Datum:
my ($now_Sekunden, $now_Minuten, $now_Stunden, $now_Monatstag, $now_Monat, $now_Jahr, $now_Wochentag) = localtime(time); $now_Monat+=1; $now_Jahr+=1900;
my @monate_kurz = ("Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez");
my @monate_lang = ("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");

my $ua = LWP::UserAgent->new;
$ua->agent('Mozilla/5.0');
$|=1;

my $dbh;
if (!$debug) {
	$dbh = DBI->connect("DBI:mysql:denkmal_org:localhost","denkmal_org","thedog")  || die "Database error: $DBI::errstr";
	$set = $Select2 = $dbh->prepare("SET NAMES utf8");
	$set->execute; $set->finish;
}





# Get Events from Eventcalendars by day:
for ($i=0; $i<$updatedays; $i++) {
	($tmp, $tmp, $tmp, $Monatstag, $Monat, $Jahr) = localtime(time + $i*86400); $Monat+=1; $Jahr+=1900;
	print "Updating $Monatstag.$Monat.$Jahr... ";

	# &update20min($Jahr,$Monat,$Monatstag, "konzerte");
	# &update20min($Jahr,$Monat,$Monatstag, "party");
	# &updateProgrammzeitung($Jahr,$Monat,$Monatstag,5);
	&updateProgrammzeitung($Jahr,$Monat,$Monatstag,6);
	print "\n";
}

# Get events from location-sites etc:
print "Updating Location-Events:";
&updateLocPage("Chez Soif");
&updateLocPage("Funambolo");
&updateLocPageViaMyspace("1.Stock | Schoolyard", "145091103");
&updateLocPageViaMyspace("Nordstern", "200687083");
&updateLocPageViaMyspace("NT Areal", "198944946");
# &updateLocPageViaMyspace("G57", "373929686");
# &updateLocPage("Gleis 13");
# &updateLocPage("Gleis 13 Myspace", "Gleis 13");
# &updateLocPage("Gleis 13 Myspace2", "Gleis 13");
# &updateLocPage("1.Stock | Schoolyard");
# &updateLocPageViaMyspace("Funambolo", "226345070");

print "\n";


# Drop cache:
print "Dropping cache...";
dropCache();


if (!$debug) {
	$dbh->disconnect();
}


print "\n";





######################################################################################

# Save a url to the meteo-pics folder
sub saveMeteo($) {
	my($url) = @_;
	system("wget -P '../pics/meteo/' '$url'");
}



## Einen Event in der Datebank eintragen
sub addEvent($$$$$$$$$$) {
	my($y,$m,$d, $zeit1, $zeit2, $wo, $was, $enabled, $nodeletion, $star) = @_;

	# Eingaben normalisieren:
	if ($y =~ /^\d{2}$/) { $y='20'.$y; }

	# Überprüfen, ob wir nicht einen Event in der Vergangenheit eintragen, das wollen wir nicht:
	if ($y<$now_Jahr) { return; }
	if ($y==$now_Jahr && $m<$now_Monat) { return; }
	if ($y==$now_Jahr && $m==$now_Monat && $d<$now_Monatstag) { return; }
	
	# Apostrophe ersetzen:
	$was =~ s/["`]/'/g;	#'"
	$was = escapeSql($was);

	# "Caps-Lock" Wörter Kleinschreiben (3 oder mehr Zeichen):
	$was =~ s/\b([A-ZÖÄÜ])([A-ZÖÄÜ]{2,})\b/$1\L$2\E/g;

	# Unnötige Leerzeichen entfernen:
	$was =~ s/(\s+)$//g;
	$was =~ s/^(\s+)//g;
	
	# Beschreibung überprüfen
	if ($was eq '') { return; }

	
	# Überprüfen, ob ein Alias für die Location besteht:
	$wo = doAlias($wo);
	if ($wo eq "block") { return; }

	# Wenn es an diesem tag in dieser Location schon einen protected-Eintrag gibt, den Event _nicht_ einfügen
	if ($debug) {
		print "\nADD: $y-$m-$d $zeit1, $wo -- $was\n";
		
	} else {
		$count = $dbh->prepare("SELECT count(*) FROM events WHERE (datum = '$y-$m-$d') and (ort = '" .escapeSql($wo). "') and (nodeletion = '1')"); $count->execute;
		$num=$count->fetchrow; $count->finish();

		
		if ($num == 0) {
			# Schauen, ob genau dieser Eintrag schon existiert:
			my $count = $dbh->prepare("SELECT count(*) FROM events WHERE (datum = '$y-$m-$d') and (ort = '" .escapeSql($wo). "') and (beschreibung = '$was') and (zeit = '$zeit1')"); $count->execute;
			my $num=$count->fetchrow; $count->finish();

			# Wenn genau dieser Eintrag noch nicht existiert, dann eintragen:
			if ($num==0) {
				# Schon existierende Einträge an diesem Tag in dieser Location in der DB löschen:
				$del = $dbh->prepare("DELETE FROM events WHERE ((datum = '$y-$m-$d') and (ort = '" .escapeSql($wo). "') and (nodeletion = '0'))");
				$del->execute;
				$del->finish;

				
				# Eintrag hinzufügen:
				$zeit1 = zeit_sql($zeit1);
				$zeit2 = zeit_sql($zeit2);
				if ($zeit2) {
					$ins = $dbh->prepare("INSERT INTO events (datum, zeit, zeit2, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$zeit2','" .escapeSql($wo). "','$was','$enabled','$nodeletion','$star')");
				} else {
					$ins = $dbh->prepare("INSERT INTO events (datum, zeit, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','" .escapeSql($wo). "','$was','$enabled','$nodeletion','$star')");
				}
				$ins->execute;
				$ins->finish;

			# Genau dieser Eintrag existiert schon:
			} else {

			}
		}
	}

	return 1;
}




# Get events from a location-page:
sub updateLocPage() {
	my($locname, $locnameInternal) = @_;
	my $programmurl;
	if ($locname eq 'Chez Soif') { $programmurl = "http://www.chezsoif.ch/unsereveranstaltungen.html"; }
	elsif ($locname eq 'Gleis 13') { $programmurl = "http://www.gleis13.com/index.php?id=4"; }
	elsif ($locname eq '1.Stock | Schoolyard') { $programmurl = "http://www.schoolyard.ch/home.php?pgnum=11&section=1"; }
	elsif ($locname eq 'Gleis 13 Myspace') { $programmurl = "http://blog.myspace.com/index.cfm?fuseaction=blog.ListAll&friendID=153431668"; }
	elsif ($locname eq 'Gleis 13 Myspace2') { $programmurl = "http://www.myspace.com/gleis13"; }
	elsif ($locname eq 'Funambolo') { $programmurl = "http://www.funambolo.ch/Programm.html"; }
	 print " $locname";
	 
	 if (!$locnameInternal) { $locnameInternal = $locname; }
	 my $numevts=0;
	 
	   $req = new HTTP::Request (GET => $programmurl);
	   $req->content_type("text/xml; charset=utf-8");
	   my $response = $ua->request($req);
	   if ($response->is_success) {
			my $src=$response->content;
			# Von latin1 nach UTF-8 konvertieren:
			Encode::from_to($src, "iso-8859-1", "utf8");
			$src=~s/[\n\r\t]/ /g;
			$src=decode_entities($src);
			# Events suchen:
			if ($locname eq 'Chez Soif') {
				while ($src =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}) ([^<]+)##) {
					my $d=$1; my $m=$2; my $y=$3;
					my $was = $4;
					my $wann = '21:00';
					# Den Eintrag der DB hinzufügen:
					if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
						$numevts++;
					}
				}
			} elsif ($locname eq 'Gleis 13') {
				if ($src =~ m#<!--  Text: \[begin\] -->(.+?)<!--  Text: \[end\] -->#) {
					$src = $1;
					my @eventsrcs = split('<p class="bodytext"> </p>', $src);
					foreach $eventsrc (@eventsrcs) {
						$eventsrc =~ s/<\/p><p class="bodytext">/ /g;
						$eventsrc =~ s/<[^>]+>//g;
						if ($eventsrc =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}) ([^<]+)##) {
							my $d=$1; my $m=$2; my $y=$3;
							my $was = $4;
							my $wann = '21:00';
							# Den Eintrag der DB hinzufügen:
							if (addEvent($y,$m,$d, $wann, '', $locnameInternal, $was, '0', '0', '1') == 1) {
								$numevts++;
							}
						} elsif ($eventsrc =~ s#(\d{1,2})\.(\d{1,2})\. ([^<]+)##) {
							my $d=$1; my $m=$2; my $y='2007';
							my $was = $3;
							my $wann = '21:00';
							# Den Eintrag der DB hinzufügen:
							if (addEvent($y,$m,$d, $wann, '', $locnameInternal, $was, '0', '0', '1') == 1) {
								$numevts++;
							}
						}
					}
				}
			} elsif ($locname eq '1.Stock | Schoolyard') {
				if ($src =~ m#src = 'slashes2.gif'/>(.+?)<div id = "navigation">#) {
					$src = $1;
					my @eventsrcs = split("<span class = 'strong'>", $src);
					foreach $eventsrc (@eventsrcs) {
						$eventsrc =~ s/<\/p>\s*<p>/ /g;
						$eventsrc =~ s/<[^>]+>//g;
						$eventsrc =~ s/\s+/ /g;
						if ($eventsrc =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}), türe (\d{1,2}):(\d{2}) (.+)##i) {
							my $d=$1; my $m=$2; my $y=$3;
							my $wann = "$4:$5";
							my $was = $6;
							$was =~ s/"//g;	#"
							$was =~ s/ \| /: /g;
							if ($was !~ /^tba\b/) {
								# Den Eintrag der DB hinzufügen:
								if (addEvent($y,$m,$d, $wann, '', $locnameInternal, $was, '0', '0', '1') == 1) {
									$numevts++;
								}
							}
						}
					}
				}
			} elsif ($locname eq 'Gleis 13 Myspace') {
				while ($src =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}) ([^<]+)##) {
					my $d=$1; my $m=$2; my $y=$3;
					my $was = $4;
					my $wann = '21:00';
					# Den Eintrag der DB hinzufügen:
					if (addEvent($y,$m,$d, $wann, '', $locnameInternal, $was, '0', '0', '1') == 1) {
						$numevts++;
					}
				}
			} elsif ($locname eq 'Gleis 13 Myspace2') {
				if ($src =~ m#<span class="text">Upcoming Events:(.+?)</span>#) {
					$src = $1;
					while ($src =~ s#\w{2}\.(\d{1,2})\.(\d{1,2})\.(\d{2,4}) ([^<]+)##) {
						my $d=$1; my $m=$2; my $y=$3;
						my $was = $4;
						my $wann = '21:00';
						# Den Eintrag der DB hinzufügen:
						if (addEvent($y,$m,$d, $wann, '', $locnameInternal, $was, '0', '0', '1') == 1) {
							$numevts++;
						}
					}
				}
			} elsif ($locname eq 'Funambolo') {
				if ($src =~ m#<h2>Programm:</h2>(.+)</body>#) {
					$src = $1;
					$src =~ s#<p>\s*</p>##g;
					# "<h3>Sonntag 25. Mai</h3><p>Warme K&uuml;che und Musik<br>ab 16 Uhr</p>"
					while ($src =~ s#<h3>\w+?\s+(\d+)\.\s+(\w+?)</h3>\s+<p>(.+?)</p>##) {
						my $d=$1; my $m=$2; my $y=$now_Jahr;
						my $was = $3;
						my $wann = '22:00';
						if ($was =~ s/ab\s+(\d+)\s+Uhr//i) { $wann = "$1:00"; }
						$was =~ s#<br>\s+#: #g;
						$was =~ s#<br>$##g;
						@m = grep(($monate_lang[$_] eq $m), 0..$#monate_lang);
						$m = $m[0]+1;
						if ($m < $now_Monat) { $y++; }
						if (addEvent($y,$m,$d, $wann, '', $locnameInternal, $was, '0', '0', '1') == 1) {
							$numevts++;
						}
					}
				}
			}
			
	   }
	 print "[$numevts] ";

}


# Get events from a myspace-location-page:
sub updateLocPageViaMyspace($$) {
	my($locname, $myspaceid) = @_;

	my $programmurl = "http://collect.myspace.com/index.cfm?fuseaction=bandprofile.listAllShows&friendid=$myspaceid";
	print " $locname";
	my $numevts=0;
	 
	   $req = new HTTP::Request (GET => $programmurl);
	   #$req->content_type("text/xml; charset=utf-8");
	   my $response = $ua->request($req);
	   if ($response->is_success) {
			my $src=$response->content;
			# Von latin1 nach UTF-8 konvertieren:
			#Encode::from_to($src, "iso-8859-1", "utf8");
			$src=~s/[\n\r\t]/ /g;
			$src=decode_entities($src);
			# Events suchen:
			while ($src =~ s#<form action="http://events\.myspace\.com/(.+?)</form>##) {
				$eventhtml = $1;
				if ($eventhtml =~ m#<input type="hidden" name="calEvtLocation" value="(.+?)">.*<input type="hidden" name="calEvtDateTime" value="(\d{1,2})-(\d{2})-(\d{4}) (\d{1,2}):(\d{2})"># ) {
					my $m=$2; my $d=$3; my $y=$4;
					my $wann = $5 .':'. $6;
					my $was = $1;
					$was =~ s/at $locname basel//ig;
					$was =~ s/at $locname//ig;
					$was =~ s/^\s+//; $was =~ s/\s+$//;
					
					if ($locname eq "1.Stock | Schoolyard") {
						$was =~ s/\[(.+?)\]/($1)/g;
					}

					if ($was =~ /\btba\b/i) {
					} else {
						#Den Eintrag der DB hinzufügen:
						if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
							$numevts++;
						}
					}
				}
			}

		}
	 print "[$numevts] ";


}



## Bringt die Event-Datenbank vom angegebenen Datum auf den neusten Stand
## 20min.ch
sub update20min() {
 my($y, $m, $d, $kategorie) = @_;
 my $datumgesucht = ymd220min($y,$m,$d);
 my $url = "http://www.20min.ch/ausgehen/$kategorie/?dates=$datumgesucht+00:00:00|$datumgesucht+23:59:59&region=BS&genre=all";
 print "20min($kategorie)";
 my $numevts=0;
 
   $req = new HTTP::Request (GET => $url);
   $req->content_type("text/xml; charset=utf-8");
   my $response = $ua->request($req);
   if ($response->is_success) {
    $src=$response->content;
	# Von latin1 nach UTF-8 konvertieren:
	Encode::from_to($src, "iso-8859-1", "utf8");
    $src=~s/[\n\r\t]//g;
	$src=decode_entities($src);
	while ($src =~ s/<tr class="even" height="18" valign="bottom">\s*<td[^>]*>.*?<\/td>\s*<td[^>]*><a[^>]*><b>(.+?)<\/b><\/a><\/td>\s*<td[^>]*>.*?<\/td>\s*<\/tr><tr[^>]*><td[^>]*>.*?\/ (.+?)<\/td>\s*<td[^>]*>\s*<a[^>]*>(.+?)\s*<\/a><\/td>//) {
		$was = $1; $wann = $2; $wo = $3;
		if ($wo =~ /^(.+),[^,]*$/) { $wo = $1; }
		$wo =~ s/<.+?>//g;

		# Den Eintrag der DB hinzufügen:
		&addEvent($y,$m,$d, $wann, '', $wo, $was, '1', '0', '0');
		$numevts++;

	}
   }
 print "[$numevts] ";
}




## Bringt die Event-Datenbank vom angegebenen Datum auf den neusten Stand
## programmzeitung.ch
sub updateProgrammzeitung() {
 my($y, $m, $d, $rubrik) = @_;
 my $datumgesucht = ymd2pz($y,$m,$d);
 my $url = "http://www.programmzeitung.ch/index.cfm?Datum_von=$datumgesucht&Datum_bis=$datumgesucht&Rubrik=$rubrik&uuid=2BCD9733D9D9424C4EF093B3E35CB44B";
 print "Programmzeitung($rubrik)";
 my $numevts=0;
 
   $req = new HTTP::Request (GET => $url);
   $req->content_type("text/xml; charset=utf-8");
   my $response = $ua->request($req);
   if ($response->is_success) {
    $src=$response->content;
    $src=~s/[\n\r\t]//g;
	$src=decode_entities($src);
	while ($src =~ s/<div class="contentrahmen"><div class="veranstaltung">(.+?)<\/div><div class="ort">(.+?)<\/div><div class="zeit">(.+?)<\/div><\/div>//) {
		$was = $1; $wo = $2; $wann = $3;
		if ($was !~ /^Keine Veranstaltung in dieser Rubrik zum heutigen Datum gefunden./i) {
			$wann1=$wann2='';
			if ($was =~ /^<b>(.+?)<\/b>\s*([^\s].+)$/) { $was = "$1: $2"; }
			elsif ($was =~ /^<b>(.+?)<\/b>\s*$/) { $was = "$1"; }
			$was =~ s/<.+?>//g;
			if ($wo =~ /^(.+),[^,]*$/) { $wo = $1; }
			$wo =~ s/<.+?>//g;
			$wo =~ s/ \[.+\]$//g;
			if ($wann =~ /^(\d{1,2}[\.:]\d{2}) - (\d{1,2}[\.:]\d{2})$/) { $wann1=zeit($1); $wann2=zeit($2); }
			elsif ($wann =~ /^(\d{1,2}[\.:]\d{2}) \|/) { $wann1=zeit($1); }
			elsif ($wann =~ /^(\d{1,2}[\.:]\d{2})$/) { $wann1=zeit($1); }
			elsif ($wann =~ /^(\d{1,2}[\.:]\d{1})$/) { $wann1=zeit($1. "0"); }
			elsif ($wann =~ /^(\d{1,2})$/) { $wann1=zeit($1 . "d:00"); }

			# Den Eintrag der DB hinzufügen:
			&addEvent($y,$m,$d, $wann1, $wann2, $wo, $was, '1', '0', '0');
			$numevts++;
		}

	}
   }
 print "[$numevts] ";
}



# Überprüfen, ob ein Alias für die Location besteht:
sub doAlias() {
	local($loc) = @_;
	local $maybealias = 1;
	local $i=0;

	if ($debug) { $maybealias=0; }

	while ($maybealias == 1) {
		$Select2 = $dbh->prepare("SELECT * FROM locations WHERE (name = '" .escapeSql($loc). "') and (alias is not null) and (enabled = '1')");
		$Select2->execute; 
		$Row2=$Select2->fetchrow_hashref;
		if ($Row2) { 
			if ($Row2->{alias} eq "block") { return "block"; }
			$loc = $Row2->{alias};
		} else {
			$maybealias = 0;
		}
		$Select2->finish();
		$i++;
		if ($i>5) { $maybealias = 0; }
	}
	return $loc;
}



## Gibt einem String UTF8-Flag:
sub decode_entities() {
	$str = $_[0];
	$str=~s/&nbsp;/ /g;
	$str=~s/&ndash;/-/g;
	$str=~s/&amp;/&/g;
	$str=~s/&auml;/ä/g;
	$str=~s/&eacute;/é/g;
	$str=~s/&egrave;/è/g;
	$str=~s/&ntilde;/ñ/g;
	$str=~s/&ouml;/ö/g;
	$str=~s/&uuml;/ü/g;
	$str=~s/&quot;/"/g;	#"
	$str=~s/&Ouml;/Ö/g;
	return $str;
}



## Zeit formatieren:
sub zeit() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2/g;
 return $zeit;
}

## Zeit nach SQL formatieren:
sub zeit_sql() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2:00/g;
 return $zeit;
}

## Datum nach Programmzeitung formatieren:
sub ymd2pz{
 my($y, $m, $d) = @_;
  if ($d =~ /^\d$/) { $d="0$d"; }
  if ($m =~ /^\d$/) { $m="0$m"; }
  if ($y =~ /^\d\d$/) { $y="19$y"; }
  return "$d.$m.$y";
}
## Datum nach 20min formatieren:
sub ymd220min{
 my($y, $m, $d) = @_;
  if ($d =~ /^\d$/) { $d="0$d"; }
  if ($m =~ /^\d$/) { $m="0$m"; }
  if ($y =~ /^\d\d$/) { $y="19$y"; }
  return "$y-$m-$d";
}



## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
 my($y, $m, $d) = @_;
  if ($m < 3) { $f = -1; } else { $f = 0; }
  return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
}


# Escapes a string to be savely used in a SQL-query
sub escapeSql($) {
	my ($str) = @_;
	$str =~ s/\\/\\\\/g;
	$str =~ s/'/\\'/g;
	return $str;
}



	

# Deletes all cache-rows in DB, so everything is new calculated:
sub dropCache() {
	if (!$debug) {
		my $del = $dbh->prepare("DELETE FROM cache WHERE (1)");
		$del->execute;
		$del->finish;
	}
}