# User Passwort-check

 $liid = "denkmalorg";
 
 $q_user = $query->param("user");
 $q_pass = $query->param("pass");
 $q_action = $query->param("action");

 # Cookies:
 $exp_date = "Thu, 01-Jan-2020 00:00:00 GMT";
 $exp_date_remove = "Thu, 01-Jan-1970 00:00:00 GMT";
 $cookiepath = "/";


 if ($q_user && $q_pass && $q_action eq "login") {
  $user=$q_user;
  $pass_hash=md5_base64($q_pass);
 } else {
  %cookies = &getCookies;
  foreach $name (keys %cookies) {
  	if ($name eq  "li1".$liid) { 
  		$user = $cookies{$name};   
	}
	if ($name eq  "li2".$liid) { 
		$pass_hash = $cookies{$name};
	}
  }
 }


 $Select = $dbh->prepare("SELECT * FROM user where (name = '$user')");
 $Select->execute();
 if ($Select) {
	$Row = $Select->fetchrow_hashref;
	if (md5_base64($Row->{passwort}) eq $pass_hash) {
		$authuser=$Row; 
	}
 }
 $Select->finish;


 if ($authuser) {
  # Set cookies for new authenticated users:
  if ($q_user && $q_pass && $q_action eq "login") {
   &setCookie("li1".$liid, "$user",  $exp_date);
   &setCookie("li2".$liid, "$pass_hash",  $exp_date);
  }

  # Set cookies to log out:
  if ($q_action eq "logout") {
   &setCookie("li1".$liid, "",  $exp_date_remove);
   &setCookie("li2".$liid, "",  $exp_date_remove);
   $user=""; $pass_hash=""; $authuser="";
  }
 }



 if ($authuser->{level} >= $auth_must_type) { 
  print $header;
  print "<i>Authenticated as $authuser->{name} ($authuser->{level}). <a href=\"?action=logout\">Logout</a></i><br>";
 } else {
  print $header;
  print "<form method=\"POST\">";
  print "<input type=\"hidden\" name=\"action\" value=\"login\"><br>";
  print "User: <input type=\"text\" name=\"user\"><br>";
  print "Pass: <input type=\"password\" name=\"pass\"><br>";
  print "<input type=\"submit\" value=\"Login\"><br>";
  print "</form>";
  exit;
 }






sub setCookie {
	local($name, $value, $expiration) = @_;

	print "Set-Cookie: ";
	print ($name, "=", $value, "; expires=", $expiration, "; path=", $cookiepath, "\n");
}


sub getCookies {
	local(@rawCookies) = split (/; /,$ENV{'HTTP_COOKIE'});
	local(%cookies);
	foreach(@rawCookies){
	    ($key, $val) = split (/=/,$_);
	    $cookies{$key} = $val;
	} 
	return %cookies; 
}


